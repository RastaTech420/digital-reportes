package com.digital.reportes;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Toast;

import com.digital.reportes.objetos.Constantes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
	
	private GoogleMap map;
	Geocoder geocoder;
	List<Address> addresses;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		Toast.makeText(getApplicationContext(), "Esta pantalla se cerrará automáticamente al ubicar la posición.", Toast.LENGTH_LONG).show();

	}
	

	@Override
	public void onMapReady(GoogleMap googleMap) {
		map = googleMap;

		try {
			String latiActual = MainActivity.latitud.toString();
			String longiActual =MainActivity.longitud.toString();
			LatLng miLatLong= new LatLng(Double.parseDouble(latiActual), Double.parseDouble(longiActual));
			map.addMarker(new MarkerOptions().position(miLatLong).title("Mi ubicación actual"));
			CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(miLatLong, 17);
			map.animateCamera(animacionZoom);

			GoogleMapOptions options = new GoogleMapOptions();
			options.zOrderOnTop(true);

			map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
				@Override
				public void onMapClick(LatLng latLng) {

					map.clear();
					geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

					map.addMarker(new MarkerOptions().position(latLng).title("Nueva ubicación actual"));
					CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(latLng, 17);
					map.animateCamera(animacionZoom);
					Constantes.OBJETO_REPORTE.setLatitud(String.valueOf(latLng.latitude));
					Constantes.OBJETO_REPORTE.setLongitud(String.valueOf(latLng.longitude));

					try
					{
						addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 100);
						String direccion = addresses.get(0).getAddressLine(0);
						String ciudad = addresses.get(0).getLocality();
						String estado = addresses.get(0).getAdminArea();
						String pais = addresses.get(0).getCountryName();
						String codigopostal= addresses.get(0).getPostalCode();
						String   direccionActual = direccion+", "+ciudad+", "+estado+", " +pais ;
						Constantes.OBJETO_REPORTE.setDireccion(direccionActual);


						new CountDownTimer(2000, 1000) {
							public void onTick(long millisUntilFinished) {

							}

							public void onFinish() {
								finish();
							}

						}.start();



					}catch (Exception e){
						Toast.makeText(getApplicationContext(), "Ha ocurrido un problema a la hora de obtener la dirección actual, por favor intente nuevamente...", Toast.LENGTH_LONG).show();

					}

				}
			});
		}catch (Exception e){
			Toast.makeText(this, "No se ha podido recuperar la ubicación", Toast.LENGTH_SHORT).show();
		}


	}
}
