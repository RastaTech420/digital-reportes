package com.digital.reportes;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.digital.reportes.FragmentsMain.CuentaFragment;
import com.digital.reportes.FragmentsMain.NotificacionesFragment;
import com.digital.reportes.FragmentsMain.ReportesFragment;
import com.digital.reportes.FragmentsMain.UbicacionesFragment;
import com.digital.reportes.PruebaINE.ActividadesExtraFragment;
import com.digital.reportes.PruebaINE.INEFragment;
import com.digital.reportes.PruebaINE.IncidenciaFragment;
import com.digital.reportes.ReportesFragment.DetalleReporteMoctezumaFragment;
import com.digital.reportes.ReportesFragment.NuevoReporteFragment;
import com.digital.reportes.ReportesFragment.ProbandoFragment;
import com.digital.reportes.ReportesFragment.ReporteListadoMoctezumaFragment;
import com.digital.reportes.ReportesFragment.ReporteListadoSupervisorFragment;
import com.digital.reportes.ViewReporteFinal.DetalleReporteFragment;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
implements ReportesFragment.OnFragmentInteractionListener,
           UbicacionesFragment.OnFragmentInteractionListener,
           NotificacionesFragment.OnFragmentInteractionListener,
           CuentaFragment.OnFragmentInteractionListener,
           NuevoReporteFragment.OnFragmentInteractionListener,
           DetalleReporteFragment.OnFragmentInteractionListener,
           INEFragment.OnFragmentInteractionListener,
            IncidenciaFragment.OnFragmentInteractionListener,
            ActividadesExtraFragment.OnFragmentInteractionListener,
            ProbandoFragment.OnFragmentInteractionListener,
            ReporteListadoMoctezumaFragment.OnFragmentInteractionListener,
            DetalleReporteMoctezumaFragment.OnFragmentInteractionListener,
            ReporteListadoSupervisorFragment.OnFragmentInteractionListener {

    public static String id, nombre_usuario, tipo_usuario, direccion_completa;
    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    Geocoder geocoder;
    List<Address> addresses;
    public static Double latitud = null;
    public static Double longitud= null;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.navigation_reportes:

                    Fragment reporteListadoSupervisorFragment = new ReporteListadoSupervisorFragment();
                    Fragment reporteListadoMoctezumaFragment = new ReporteListadoMoctezumaFragment();
                    Fragment probando                        = new ProbandoFragment();

                    if (tipo_usuario.equals("3")){ //moctezuma #3
                        switchFragments(reporteListadoMoctezumaFragment);

                    }else if (tipo_usuario.equals("0")){ //supervisor #0
                        switchFragments(reporteListadoSupervisorFragment);
                        //switchFragments(probando);
                    }

                    return true;
                case R.id.navigation_ubicaciones:
                    UbicacionesFragment ubicacionesFragment = new UbicacionesFragment();
                    switchFragments(ubicacionesFragment);
                    return true;

                case R.id.navigation_notificaciones:
                    NotificacionesFragment notificacionesFragment = new NotificacionesFragment();
                    switchFragments(notificacionesFragment);
                    return true;

                /*case R.id.navigation_cuenta:
                    INEFragment ineFragment = new INEFragment();
                    switchFragments(ineFragment);
                    return true;
*/
                default:
                    break;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datosLogin();


        lastKnowLocation();

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        gpsCheckPermission();

        //Aquí inicializo la vista para poder hacer switch entre el layout con o sin conexión
        checkInternet();
        //checkCurrentLocation();

    }

    private void datosLogin() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        id             = bundle.getString("id");
        nombre_usuario = bundle.getString("nombre");
        tipo_usuario   = bundle.getString("tipo_usuario");
        setTitle("Bienvenido "+nombre_usuario);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_toolbar:
                Intent i  = new Intent(MainActivity.this, LoginActivity.class);
                overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);

                startActivity(i);
                finish();
            break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {}
    private void gpsCheckPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }
    private void lastKnowLocation()
    {
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        client.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>()
        {
            @Override
            public void onSuccess(Location location)
            {
                if (location != null)
                {
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();

                    gpsData(latitud, longitud);
                }
            }
        });

    }
    private void gpsData(Double latitud, Double longitud)
    {
        geocoder = new Geocoder(this, Locale.getDefault());
        try
        {
            addresses = geocoder.getFromLocation(latitud, longitud, 100);

            String direccion = addresses.get(0).getAddressLine(0);
            //String area      = addresses.get(0).getLocality();
            //String ciudad    = addresses.get(0).getAdminArea();
            String codigopostal= addresses.get(0).getPostalCode();
            //latitud = addresses.get(0).getLatitude();
            //longitud= addresses.get(0).getLongitude();


            direccion_completa = direccion+", "+codigopostal;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("ERROR UBICACION", e.getLocalizedMessage());
        }
    }
    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal, fragment);
        ft.commit();
    }
    private boolean checkInternetConnection(){
        boolean hay_WIFI = false;
        boolean hay_DatosMoviles = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();

        for (NetworkInfo info:networkInfos){
            if(info.getTypeName().equalsIgnoreCase("WIFI"))
                if (info.isConnected())
                    hay_WIFI=true;
            if (info.getTypeName().equalsIgnoreCase("MOBILE"))
                if (info.isConnected())
                    hay_DatosMoviles=true;
        }
        return hay_DatosMoviles||hay_WIFI;
    }
    public void checkInternet(){
        if (checkInternetConnection()){
            if (tipo_usuario.equals("0")){//Supervisor #0
                Fragment listadoSupervisor = new ReporteListadoSupervisorFragment();
                Fragment probando          = new ProbandoFragment();
                fragmentInicialUsuarios(R.id.contenedor_principal, listadoSupervisor);

            }else if(tipo_usuario.equals("3")){ //Moctezuma #3
                Fragment reporteMoctezuma = new ReporteListadoMoctezumaFragment();

                fragmentInicialUsuarios(R.id.contenedor_principal, reporteMoctezuma);
            }

        }
        else if (!checkInternetConnection()){
            //ErrorFragment errorFragment = new ErrorFragment();
            //switchFragments(errorFragment);
        }
    }
    private void fragmentInicialUsuarios(int id, Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(id, fragment);
        ft.commit();
    }
}
