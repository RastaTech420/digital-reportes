package com.digital.reportes.FragmentsMain;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digital.reportes.EstadisticasActivity;
import com.digital.reportes.LoginActivity;
import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import com.digital.reportes.ReportesFragment.NuevoReporteFragment;
import com.digital.reportes.SplashActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReportesFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    private TextView tvBienvenidoUsuario, tvUbicacionActual, tvIngreso;
    private LinearLayout btnNuevoReporte, btnEstadisticas;
    Geocoder geocoder;
    List<Address> addresses;
    Double latitud = null;
    Double longitud= null;
    private String ubicacion;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_reportes, container, false);

        initView(view);
        clicks();
        lastKnowLocation();


        return view;
    }


    private void initView(View view) {
        tvBienvenidoUsuario = view.findViewById(R.id.tvBienvenidoUsuario);
        tvUbicacionActual   = view.findViewById(R.id.tvUbicacionActual);
        tvIngreso           = view.findViewById(R.id.tvIngreso);
        btnNuevoReporte     = view.findViewById(R.id.btnNuevoReporte);
        btnEstadisticas     = view.findViewById(R.id.btnEstadisticas);


    }
    private void clicks() {
        btnNuevoReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment nuevoReporteFragment = new NuevoReporteFragment();
                switchFragments(nuevoReporteFragment);
            }
        });
        btnEstadisticas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EstadisticasActivity.class);
                startActivity(intent);
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void lastKnowLocation()
    {
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(getActivity());
        client.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>()
        {
            @Override
            public void onSuccess(Location location)
            {
                if (location != null)
                {
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();

                    gpsData(latitud, longitud);
                }
            }
        });

    }
    private void gpsData(Double latitud, Double longitud)
    {
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try
        {
            addresses = geocoder.getFromLocation(latitud, longitud, 100);

            String direccion = addresses.get(0).getAddressLine(0);
            //String area      = addresses.get(0).getLocality();
            String ciudad    = addresses.get(0).getAdminArea();
            String codigopostal= addresses.get(0).getPostalCode();
            //latitud = addresses.get(0).getLatitude();
            //longitud= addresses.get(0).getLongitude();


            ubicacion = direccion+", "+codigopostal;

            tvUbicacionActual.setText(tvUbicacionActual.getText().toString()+ubicacion);
            tvBienvenidoUsuario.setText(tvBienvenidoUsuario.getText().toString()+MainActivity.nombre_usuario);

            String currentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault()).format(new Date());
            tvIngreso.setText(tvIngreso.getText().toString()+currentDate);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal, fragment);
        ft.commit();
    }
}
