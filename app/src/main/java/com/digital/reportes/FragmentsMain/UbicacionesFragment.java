package com.digital.reportes.FragmentsMain;

import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.digital.reportes.R;
import com.digital.reportes.objetos.Constantes;
import com.digital.reportes.objetos.Coordenadas;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class UbicacionesFragment extends Fragment {
    
    View view;
    LinearLayout linearMapaUbicaciones, linearBotonesOpciones;
    TextView tituloUbicaciones;
    LinearLayout btnAsignacionesCompletadas,btnAsignacionesNoCompletadas, btnUbicacionAsignaciones;
    Button btnVolverMenu;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    List<Coordenadas> COORDENADAS_COMPLETAS = new ArrayList<>();
    List<Coordenadas> COORDENADAS_NO_COMPLETAS = new ArrayList<>();
    List<Coordenadas> COORDENADAS_TODAS = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_ubicaciones, container, false);
        initComponets();
        clicks();

        poblarCompletas();
        poblarIncompletas();
        poblarTodas();



        
        return view;
    
    }

     public void poblarCompletas(){

         AsyncHttpClient client = new AsyncHttpClient();
         String url = Constantes.URL_PRINCIPAL + "mostrar_rep_con_status.php";
         client.get(url,null,new AsyncHttpResponseHandler(){

             @Override
             public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                 if (statusCode == 200){
                     obtieneJSON(new String(responseBody));
                 }
             }

             @Override
             public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

             }
         });



     }

    private void obtieneJSON(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);

                String nombre               = js.getString("os_nombre");
                String status               = js.getString("os_estatus");
                String latitud              = js.getString("rep_latitud");
                String longitud             = js.getString("rep_longitud");

                if(status.equals("1")){
                    COORDENADAS_COMPLETAS.add(new Coordenadas(latitud, longitud, nombre));
                }


            }
        }catch (Exception e){

        }
    }


    public void poblarIncompletas(){

    }

    public void poblarTodas(){

    }
    
    private void clicks() {
        btnAsignacionesCompletadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tituloUbicaciones.setVisibility(View.GONE);
                linearBotonesOpciones.setVisibility(View.GONE);
                linearMapaUbicaciones.setVisibility(View.VISIBLE);
                btnVolverMenu.setVisibility(View.VISIBLE);
    
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
    
                        mMap.clear();

                        try{

                        for (int i =0; i<COORDENADAS_COMPLETAS.size(); i++){

                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(COORDENADAS_COMPLETAS.get(i).getLatitud()),Double.parseDouble(COORDENADAS_COMPLETAS.get(i).getLongitud()))).title(COORDENADAS_COMPLETAS.get(i).getDescripcion()));


                        }
                            CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(COORDENADAS_COMPLETAS.get(0).getLatitud()),Double.parseDouble(COORDENADAS_COMPLETAS.get(0).getLongitud())), 17);
                            mMap.animateCamera(animacionZoom);

                        }catch (Exception e){
                            Toast.makeText(getActivity(), "No hay órdenes de servicio finalizadas aún", Toast.LENGTH_SHORT).show();


                        }




    /*
                        LatLng pA = new LatLng(17.949874, -92.952288);
                        LatLng pB = new LatLng(18.032719, -92.925256);
                        LatLng pC = new LatLng(18.012287, -92.864913);
                        LatLng pD = new LatLng(18.000413, -92.999612);
                        mMap.addMarker(new MarkerOptions().position(pA).title("Sabina, Villahermosa, Tab."));
                        mMap.addMarker(new MarkerOptions().position(pB).title("Nacajuca, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pC).title("Centro, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pD).title("Centro, Tabasco"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(pA, 17);
                        mMap.animateCamera(animacionZoom);



                    */

                    }
                });
    
            }
        });
    
        btnVolverMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tituloUbicaciones.setVisibility(View.VISIBLE);
                linearBotonesOpciones.setVisibility(View.VISIBLE);
                linearMapaUbicaciones.setVisibility(View.GONE);
                btnVolverMenu.setVisibility(View.GONE);
    
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
            
                        mMap.clear();
                        LatLng pA = new LatLng(17.949874, -92.952288);
                        LatLng pB = new LatLng(18.032719, -92.925256);
                        LatLng pC = new LatLng(18.012287, -92.864913);
                        LatLng pD = new LatLng(18.000413, -92.999612);
                        mMap.addMarker(new MarkerOptions().position(pA).title("Sabina, Villahermosa, Tab."));
                        mMap.addMarker(new MarkerOptions().position(pB).title("Nacajuca, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pC).title("Centro, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pD).title("Centro, Tabasco"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(pA, 17);
                        mMap.animateCamera(animacionZoom);
                    }
                });
                
            }
        });
    
        btnAsignacionesNoCompletadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tituloUbicaciones.setVisibility(View.GONE);
                linearBotonesOpciones.setVisibility(View.GONE);
                linearMapaUbicaciones.setVisibility(View.VISIBLE);
                btnVolverMenu.setVisibility(View.VISIBLE);
    
    
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
            
                        mMap.clear();
                        Toast.makeText(getActivity(), "Ubicaciones de prueba", Toast.LENGTH_SHORT).show();

                        LatLng pA = new LatLng(17.949874, -92.952288);
                        LatLng pB = new LatLng(18.032719, -92.925256);
                        LatLng pC = new LatLng(18.012287, -92.864913);
                        LatLng pD = new LatLng(18.000413, -92.999612);
                        mMap.addMarker(new MarkerOptions().position(pA).title("Sabina, Villahermosa, Tab."));
                        mMap.addMarker(new MarkerOptions().position(pB).title("Nacajuca, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pC).title("Centro, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pD).title("Centro, Tabasco"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(pC, 17);
                        mMap.animateCamera(animacionZoom);
                    }
                });
            
            }
        });
    
    
        btnUbicacionAsignaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tituloUbicaciones.setVisibility(View.GONE);
                linearBotonesOpciones.setVisibility(View.GONE);
                linearMapaUbicaciones.setVisibility(View.VISIBLE);
                btnVolverMenu.setVisibility(View.VISIBLE);
    
    
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
            
                        mMap.clear();
                        Toast.makeText(getActivity(), "Ubicaciones de prueba", Toast.LENGTH_SHORT).show();

                        LatLng pA = new LatLng(17.949874, -92.952288);
                        LatLng pB = new LatLng(18.032719, -92.925256);
                        LatLng pC = new LatLng(18.012287, -92.864913);
                        LatLng pD = new LatLng(18.000413, -92.999612);
                        mMap.addMarker(new MarkerOptions().position(pA).title("Sabina, Villahermosa, Tab."));
                        mMap.addMarker(new MarkerOptions().position(pB).title("Nacajuca, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pC).title("Centro, Tabasco"));
                        mMap.addMarker(new MarkerOptions().position(pD).title("Centro, Tabasco"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(pD, 17);
                        mMap.animateCamera(animacionZoom);
                    }
                });
            }
        });


    }
    
    private void initComponets() {
        btnAsignacionesCompletadas = view.findViewById(R.id.btnAsignacionesCompletadas);
        btnAsignacionesNoCompletadas = view.findViewById(R.id.btnAsignacionesSinTerminar);
        btnUbicacionAsignaciones = view.findViewById(R.id.btnUbicacionesAsignadas);
        tituloUbicaciones = view.findViewById(R.id.tvTituloUbicaciones);
        linearBotonesOpciones=view.findViewById(R.id.linearBotones);
        btnVolverMenu= view.findViewById(R.id.btnVolver);
        linearMapaUbicaciones = view.findViewById(R.id.linearUbicaciones);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapaUbicaciones);
        btnVolverMenu.setVisibility(View.GONE);
    
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    
}
