package com.digital.reportes.ReportesFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.digital.reportes.R;
import com.digital.reportes.adaptadores.AdaptadorListadoReporteMoctezuma;
import com.digital.reportes.objetos.Constantes;
import com.digital.reportes.objetos.Reporte;
import com.digital.reportes.objetos.ReporteMoctezuma;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ReporteListadoMoctezumaFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private List<Reporte> listaReportes;
    private AdaptadorListadoReporteMoctezuma adaptadorListadoReporteMoctezuma;

    LinearLayout lnContenido, lnLoading;
    private SwipeRefreshLayout swipeRefreshLayout;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reporte_listado_moctezuma, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Esto se ejecuta cada vez que se realiza el gesto
                listaReportes.clear();
                obtenerDatos();
                swipeRefreshLayout.setRefreshing(false);
            }

        });

        RecyclerView recyclerView = view.findViewById(R.id.recycler_listado_moctezuma);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,true);

//        linearLayoutManager.setReverseLayout(true);
        recyclerView.setHasFixedSize(true);

        //recyclerView.setLayoutManager(linearLayoutManager);
        listaReportes = new ArrayList<>();
        adaptadorListadoReporteMoctezuma = new AdaptadorListadoReporteMoctezuma(listaReportes, getActivity());
        recyclerView.setAdapter(adaptadorListadoReporteMoctezuma);
        //listaReportes.clear();

        lnContenido = view.findViewById(R.id.lnContenido);
        lnLoading   = view.findViewById(R.id.lnLoading);

        obtenerDatos();
        return view;
    }
    private void agregarReporte(String rep_folio,
                                String rep_direccion, String rep_latitud, String rep_longitud,
                                String rep_evidencia_inicial_uno, String rep_evidencia_inicial_dos,
                                String rep_evidencia_final_uno, String rep_evidencia_final_dos,
                                String rep_firmavobo, String rep_ine_anverso, String rep_ine_reverso,
                                String rep_fkorden)
    {

        Reporte reporteMoctezuma = new Reporte();
        reporteMoctezuma.setFolio(rep_folio);
        reporteMoctezuma.setDireccion(rep_direccion);
        reporteMoctezuma.setLatitud(rep_latitud);
        reporteMoctezuma.setLongitud(rep_longitud);
        reporteMoctezuma.setFoto_evidencia_inicial_uno(rep_evidencia_inicial_uno);
        reporteMoctezuma.setFoto_evidencia_inicial_dos(rep_evidencia_inicial_dos);
        reporteMoctezuma.setFoto_evidencia_final_uno(rep_evidencia_final_uno);
        reporteMoctezuma.setFoto_evidencia_final_dos(rep_evidencia_final_dos);
        reporteMoctezuma.setFirmavobo(rep_firmavobo);
        reporteMoctezuma.setFoto_ine_anverso(rep_ine_anverso);
        reporteMoctezuma.setFoto_ine_reverso(rep_ine_reverso);
        reporteMoctezuma.setOrden(rep_fkorden);

        listaReportes.add(reporteMoctezuma);
        adaptadorListadoReporteMoctezuma.notifyDataSetChanged();
    }
    private void obtenerDatos() {
        lnLoading.setVisibility(View.VISIBLE);
        lnContenido.setVisibility(View.GONE);
        AsyncHttpClient cliente = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"mostrar_reportes.php";
        cliente.get(url, null, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Manejo de error
                Log.e("Error conexión", error.getLocalizedMessage());
            }
        });

    }

    private void obtieneJSON(String response) {
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);
                //String fkusuario = js.getString("rep_fkusuario");
                String folio     = js.getString("rep_folio");
                String direccion = js.getString("rep_direccion");
                String latitud   = js.getString("rep_latitud");
                String longitud  = js.getString("rep_longitud");
                String evidencia_incial_1 = js.getString("rep_evidencia_inicial_uno");
                String evidencia_incial_2 = js.getString("rep_evidencia_inicial_dos");
                String evidencia_final_1  = js.getString("rep_evidencia_final_uno");
                String evidencia_final_2  = js.getString( "rep_evidencia_final_dos");
                String firma              = js.getString("rep_firmavobo");
                String ine_anverso        = js.getString("rep_ine_anverso");
                String ine_reverso        = js.getString("rep_ine_reverso");
                String orden              = js.getString("rep_fkorden");

                agregarReporte(folio,
                        direccion,latitud,longitud,evidencia_incial_1,
                        evidencia_incial_2,evidencia_final_1, evidencia_final_2,
                        firma,ine_anverso,ine_reverso, orden);

                lnContenido.setVisibility(View.VISIBLE);
                lnLoading.setVisibility(View.GONE);
            }
        }catch (Exception e){
            Log.e("Error conexion", e.getLocalizedMessage());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
