package com.digital.reportes.ReportesFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.digital.reportes.CargaReportesActivity;
import com.digital.reportes.R;
import com.digital.reportes.objetos.Constantes;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class DetalleReporteMoctezumaFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private TextView tvDetalleFolio, tvDetalleDireccion;
    private ImageView imgDetalleInicialUno, imgDetalleInicialDos, imgDetalleFinalUno,
            imgDetalleFinalDos, imgDetalleFirma, imgDetalleIneUno, imgDetalleIneDos;

    String folio, clave, direccion, imginicialuno,
            imginicialdos, imgfinaluno, imgfinaldos, imgineuno, imginedos, imgfirma;

    LinearLayout lnContenidoDetalle, lnCargaDetalle;



    public DetalleReporteMoctezumaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_detalle_reporte_moctezuma, container, false);
        tvDetalleFolio = view.findViewById(R.id.tvDetalleFolio);
        tvDetalleDireccion = view.findViewById(R.id.tvDetalleDireccion);
        imgDetalleInicialUno = view.findViewById(R.id.imgDetalleInicialUno);
        imgDetalleInicialDos = view.findViewById(R.id.imgDetalleInicialDos);
        imgDetalleFinalUno = view.findViewById(R.id.imgDetalleFinalUno);
        imgDetalleFinalDos = view.findViewById(R.id.imgDetalleFinalDos);
        imgDetalleFirma    = view.findViewById(R.id.imgDetalleFirma);
        imgDetalleIneUno   = view.findViewById(R.id.imgDetalleINEUno);
        imgDetalleIneDos   = view.findViewById(R.id.imgDetalleINEDos);

        lnCargaDetalle = view.findViewById(R.id.lnLoadingDetalle);
        lnContenidoDetalle = view.findViewById(R.id.lnContenidoDetalle);


        cargaDatosVistas();
        return view;
    }




    private void cargaDatosVistas() {
        Bundle bundle = this.getArguments();
        if (bundle != null){
            folio = bundle.getString("folio");
            clave = bundle.getString("clave");
            direccion = bundle.getString("direccion");
            imginicialuno = bundle.getString("evidencia_inicial_1");
            imginicialdos = bundle.getString("evidencia_inicial_2");
            imgfinaluno   = bundle.getString("evidencia_final_1");
            imgfinaldos   = bundle.getString("evidencia_final_2");
            imgineuno     = bundle.getString("ine_anverso");
            imginedos     = bundle.getString("ine_reverso");
            imgfirma      = bundle.getString("firma");

            tvDetalleFolio.setText("Folio: "+ folio);
            tvDetalleDireccion.setText(direccion);

            Glide.with(getActivity())
                    .load(imginicialuno)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleInicialUno);

            Glide.with(getActivity())
                    .load(imginicialdos)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleInicialDos);

            Glide.with(getActivity())
                    .load(imgfinaluno)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleFinalUno);
            Glide.with(getActivity())
                    .load(imgfinaldos)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleFinalDos);

            Glide.with(getActivity())
                    .load(imgineuno)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleIneUno);

            Glide.with(getActivity())
                    .load(imginedos)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleIneDos);

            Glide.with(getActivity())
                    .load(imgfirma)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgDetalleFirma);
        }else{
            cargarWSDatos();
        }
    }

    private void cargarWSDatos() {
        lnContenidoDetalle.setVisibility(View.GONE);
        lnCargaDetalle.setVisibility(View.VISIBLE);

        AsyncHttpClient cliente = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"mostrar_reportes_id.php?id="+ CargaReportesActivity.clave;
        cliente.get(url, null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Manejo de error
                Log.e("Error conexión", error.getLocalizedMessage());
            }
        });
    }
    private void obtieneJSON(String response) {
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);
                //String fkusuario = js.getString("rep_fkusuario");
                String foliov     = js.getString("rep_folio");
                String direccionv = js.getString("rep_direccion");
                String latitudv   = js.getString("rep_latitud");
                String longitudv  = js.getString("rep_longitud");
                String evidencia_incial_1v = js.getString("rep_evidencia_inicial_uno");
                String evidencia_incial_2v = js.getString("rep_evidencia_inicial_dos");
                String evidencia_final_1v  = js.getString("rep_evidencia_final_uno");
                String evidencia_final_2v  = js.getString( "rep_evidencia_final_dos");
                String firmav              = js.getString("rep_firmavobo");
                String ine_anversov        = js.getString("rep_ine_anverso");
                String ine_reversov        = js.getString("rep_ine_reverso");
                String ordenv              = js.getString("rep_fkorden");

                tvDetalleFolio.setText("Folio: "+ foliov);
                tvDetalleDireccion.setText(direccionv);

                Glide.with(getActivity())
                        .load(evidencia_incial_1v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleInicialUno);

                Glide.with(getActivity())
                        .load(evidencia_incial_2v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleInicialDos);

                Glide.with(getActivity())
                        .load(evidencia_final_1v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleFinalUno);
                Glide.with(getActivity())
                        .load(evidencia_final_2v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleFinalDos);

                Glide.with(getActivity())
                        .load(ine_anversov)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleIneUno);

                Glide.with(getActivity())
                        .load(ine_reversov)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleIneDos);

                Glide.with(getActivity())
                        .load(firmav)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleFirma);
                lnContenidoDetalle.setVisibility(View.VISIBLE);
                lnCargaDetalle.setVisibility(View.GONE);

            }
        }catch (Exception e){
            Log.e("Error conexion", e.getLocalizedMessage());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
