package com.digital.reportes.ReportesFragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digital.reportes.BuildConfig;
import com.digital.reportes.R;
import com.digital.reportes.adaptadores.AdaptadorActividades;
import com.digital.reportes.objetos.Actividad;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class NuevoReporteFragment extends Fragment implements GoogleMap.OnMapClickListener {
    private SupportMapFragment mapFrag;
    private GoogleMap map;

    private String folio;
    private TextView tvFolio;
    private CardView cardMasImagenes, cardMasImagenesFinal;
    private LinearLayout lnImagenesAntes, lnReportes1, lnReportes2, lnFachadaLow, lnFachadaHigh,
            lnFachadaSLP, lnReportes3, lnTareasPrincipales,lnExtraFachadaLowTareas, lnReportes5, lnReportes4,
            btnFachadas, lnImagenesFinales, lnReportes6Firma, lnSuccess;
    private Button btnSiguienteReporte1, btnSiguienteReporte3, btnSiguienteReporte4, btnSiguienteReporte5,
            limpiarFirmaBoton, guardarFirmaBoton;
    
    ImageButton btnObtenerDireccion, btnFotoInicialUno,btnFotoInicialDos,btnFotoFinalUno,btnFotoFinalDos;
    ImageView vistaFotoInicialUno,vistaFotoInicialDos,vistaFotoFinalUno,vistaFotoFinalDos;
    Button btnCargarDesdeMapa;
    LinearLayout linearMapaContenedor, contenedor_incidencia_nuevoreporte;
    SignaturePad cuadroFirma;
    RecyclerView recyclerActividadesExtras;
    CheckBox chkPintura, chkMarquesinaLow, chkBotonPlanoTecate, chkLaminaEstireno, chkPortaPoster, chkRotulacionSocialAnexos,
            chkRotulacionCorteVinil, chkRotulacionNombreSobreFachada, chkBastidorLona;
    EditText cajaNumActExt, etNoEjecucion;

    private static final int SOLICITUD_FOTO_INICIAL_1 = 10;
    private static final int SOLICITUD_FOTO_INICIAL_2 = 11;
    private static final int SOLICITUD_FOTO_FINAL_1 = 12;
    private static final int SOLICITUD_FOTO_FINAL_2 = 13;
    
    private Uri uriImagenIncialUno;
    private Uri uriImagenIncialDos;
    private Uri uriImagenFinalUno;
    private Uri uriImagenFinalDos;
	int casoTipoFotoSeleccion;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nuevo_reporte, container, false);
        initComponents(view);
     
        
        GoogleMapOptions options = new GoogleMapOptions();
        options.zOrderOnTop(true);
    
        mapFrag =  (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapillaNuevoReporte);
        
    
        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        map.clear();
                        map.addMarker(new MarkerOptions().position(latLng).title("Mi ubicación"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(latLng, 17);
                        map.animateCamera(animacionZoom);
    
                    }
                });

				
            }
        });
    
    

        clicks();
        cargaDialog();

        return view;
    }
    

    private void initComponents(View view) {
        linearMapaContenedor = view.findViewById(R.id.linearContenedorMapilla);
        linearMapaContenedor.setVisibility(View.GONE);
        btnCargarDesdeMapa = view.findViewById(R.id.btnCargarUbicacionMapa);
        tvFolio         = view.findViewById(R.id.tvFolio);
        cardMasImagenes      = view.findViewById(R.id.cardMasImagenes);
        lnImagenesAntes      = view.findViewById(R.id.lnImagenesAntes);
        btnSiguienteReporte1 = view.findViewById(R.id.btnSiguienteReporte1);
        lnReportes1          = view.findViewById(R.id.lnReportes1);
        lnReportes2          = view.findViewById(R.id.lnReportes2);
        lnFachadaHigh        = view.findViewById(R.id.lnFachadaHigh);
        lnFachadaLow         = view.findViewById(R.id.lnFachadaLow);
        lnFachadaSLP         = view.findViewById(R.id.lnFachadaSLP);
        btnFachadas          = view.findViewById(R.id.btnFachadas);
        lnReportes3          = view.findViewById(R.id.lnReportes3);
        lnTareasPrincipales  = view.findViewById(R.id.lnTareasPrincipales);
        btnFotoInicialUno = view.findViewById(R.id.imgButtonFotoInicialUno);
        vistaFotoInicialUno = view.findViewById(R.id.imgFotoInicialUnoVista);
        btnFotoInicialDos= view.findViewById(R.id.imgButtonFotoInicialDos);
        vistaFotoInicialDos = view.findViewById(R.id.imgFotoInicialDosVista);
        lnExtraFachadaLowTareas = view.findViewById(R.id.lnExtraFachadaLowTareas);
        btnSiguienteReporte3 = view.findViewById(R.id.btnSiguienteReporte3);
        lnReportes5          = view.findViewById(R.id.lnReportes5);
        btnFotoFinalUno      = view.findViewById(R.id.imgButtonFotoFinalUno);
        btnFotoFinalDos      = view.findViewById(R.id.imgButtonFotoFinalDos);
        vistaFotoFinalUno    = view.findViewById(R.id.imgFotoFinalUnoVista);
        vistaFotoFinalDos    = view.findViewById(R.id.imgFotoFinalDosVista);
  //      lnImagenesFinales    = view.findViewById(R.id.lnImagenesFinales);
//        cardMasImagenesFinal = view.findViewById(R.id.cardMasImagenesFinal);
        btnSiguienteReporte4 = view.findViewById(R.id.btnSiguienteReporte4);
        lnReportes4          = view.findViewById(R.id.lnReportes4);
        btnSiguienteReporte5 = view.findViewById(R.id.btnSiguienteReporte5);
        lnReportes6Firma     = view.findViewById(R.id.lnReportes6Firma);
        guardarFirmaBoton    = view.findViewById(R.id.guardarFirmaBoton);
        cuadroFirma          = view.findViewById(R.id.firmaCuadro);
        limpiarFirmaBoton    = view.findViewById(R.id.limpiarFirmaBoton);
        lnSuccess            = view.findViewById(R.id.lnSuccess);
        recyclerActividadesExtras = view.findViewById(R.id.recyclerActividadesExtras);
        cajaNumActExt        = view.findViewById(R.id.cajaNumActividadesExtras);
        contenedor_incidencia_nuevoreporte = view.findViewById(R.id.contenedor_incidencia_nuevoreporte);
        etNoEjecucion        = view.findViewById(R.id.etNoEjecucion);


        //Optimizar
        chkPintura           = view.findViewById(R.id.chkPintura);
        chkMarquesinaLow     = view.findViewById(R.id.chkMarquesinaLow);
        chkBotonPlanoTecate  = view.findViewById(R.id.chkBotonPlanoTecate);
        chkLaminaEstireno    = view.findViewById(R.id.chkLaminaEstireno);
        chkPortaPoster       = view.findViewById(R.id.chkPortaPoster);
        chkRotulacionSocialAnexos = view.findViewById(R.id.chkRotulacionSocialAnexos);
        chkRotulacionCorteVinil   = view.findViewById(R.id.chkRotulacionCorteVinil);
        chkRotulacionNombreSobreFachada = view.findViewById(R.id.chkRotulacionNombreSobreFachada);
        chkBastidorLona                 = view.findViewById(R.id.chkBastidorLona);


    }
   
    private void clicks() {

        cardMasImagenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnImagenesAntes.setVisibility(View.VISIBLE);
            }
        });

        btnSiguienteReporte1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnReportes1.setVisibility(View.GONE);
                lnReportes2.setVisibility(View.VISIBLE);
            }
        });
        btnFachadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //añadir el bottom o nuevo cardview
                lnReportes3.setVisibility(View.VISIBLE);
                lnTareasPrincipales.setVisibility(View.GONE);

            }
        });
        lnFachadaLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnExtraFachadaLowTareas.setVisibility(View.VISIBLE);
            }
        });
    
        btnCargarDesdeMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            
                linearMapaContenedor.setVisibility(View.VISIBLE);
            }
        });

        
        //FOTOS CLICKS
    
        btnFotoInicialUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(1);
            }
        });
    
    
    
        btnFotoInicialDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(2);
            }
        });

        btnFotoFinalUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(3);
            }
        });
        btnFotoFinalDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(4);
            }
        });
        btnSiguienteReporte3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!chkPintura.isChecked() || !chkMarquesinaLow.isChecked() || !chkBotonPlanoTecate.isChecked() ||
                    !chkLaminaEstireno.isChecked() || !chkPortaPoster.isChecked() || !chkRotulacionSocialAnexos.isChecked() ||
                    !chkRotulacionCorteVinil.isChecked() || !chkRotulacionNombreSobreFachada.isChecked() || !chkBastidorLona.isChecked()){

                    contenedor_incidencia_nuevoreporte.setVisibility(View.VISIBLE);
                    lnExtraFachadaLowTareas.setVisibility(View.GONE);

                }else{
                    lnReportes3.setVisibility(View.GONE);
                    lnReportes2.setVisibility(View.GONE);
                    lnReportes4.setVisibility(View.VISIBLE);
                }


            }
        });

        btnSiguienteReporte4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnReportes4.setVisibility(View.GONE);
                lnReportes5.setVisibility(View.VISIBLE);
            }
        });
        btnSiguienteReporte5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnReportes5.setVisibility(View.GONE);
                lnReportes6Firma.setVisibility(View.VISIBLE);
            }
        });

        guardarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Firma guardada con éxito!", Toast.LENGTH_SHORT).show();
                lnSuccess.setVisibility(View.VISIBLE);
                lnReportes6Firma.setVisibility(View.GONE);
                tvFolio.setVisibility(View.GONE);

                //Aquí guardar toda la información adquirida del reporte

            }
        });
        limpiarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cuadroFirma.clear();
            }
        });


        cuadroFirma.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                guardarFirmaBoton.setEnabled(true);
                limpiarFirmaBoton.setEnabled(true);
            }

            @Override
            public void onClear() {
                guardarFirmaBoton.setEnabled(false);
                limpiarFirmaBoton.setEnabled(false);
            }
        });

        cajaNumActExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (cajaNumActExt.getText().toString() != null ) {
                        int numActividades = Integer.parseInt(cajaNumActExt.getText().toString());
                        generarTarjetasActividades(numActividades);

                    }else if( cajaNumActExt.getText().toString() == ""){
                        generarTarjetasActividades(0);

                    }

                }catch (Exception e) {
                    Log.e("ERROR", e.getLocalizedMessage());
                }

            }
        });





    }
    
    private void obtenerFoto(final int num_foto) {
    
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);
    
        builder.setView(viewDialog);
    
        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);
    
        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);
        
        
        final AlertDialog dialog = builder.create();
        
        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				casoTipoFotoSeleccion=1;
	
				verificarPermisosCamaraMemoria(num_foto, 1);
				dialog.dismiss();
	
	
			}
        });
    
        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				casoTipoFotoSeleccion=2;
	
				verificarPermisosCamaraMemoria(num_foto, 2);
	
				dialog.dismiss();
	
			}
        });
        
        
        
        
        
    
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                
            }
        });
    
         dialog.show();
      
    }
    
    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);
                
            }
        }
        
        
    }
    
    private void abrirCamara(int foto, int tipoCaso) {
        
        if(foto==1 && tipoCaso==1){
            
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenIncialUno);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_INICIAL_1);
			
        
        }else if(foto==2 && tipoCaso==1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenIncialDos);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_INICIAL_2);
            
        }else  if(foto==1 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
			String currentDateandTime = sdf.format(new Date());

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_uno"+currentDateandTime+".jpg");

			if (getActivity() != null) {
				uriImagenIncialUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialUno);
				startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_1);
			}else{
				Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
			}
        }else  if(foto==2 && tipoCaso==2){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
			String currentDateandTime = sdf.format(new Date());
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_dos"+currentDateandTime+".jpg");
			if (getActivity() != null){
				uriImagenIncialDos = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialDos);
				startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_2);
			}

        }else if(foto==3 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_uno"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenFinalUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalUno);
                startActivityForResult(intent, SOLICITUD_FOTO_FINAL_1);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }else if(foto==4 && tipoCaso == 2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_dos"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenFinalDos = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalDos);
                startActivityForResult(intent, SOLICITUD_FOTO_FINAL_2);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      //1 = SELECCIÓN DE ARCHIVOS
		//2 = TOMAR FOTO DESDE LA CÁMARA
    
        if (requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);
            
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
			
        }else if(requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
			try {
				Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialUno);
				btnFotoInicialUno.setVisibility(View.GONE);
				vistaFotoInicialUno.setVisibility(View.VISIBLE);
				vistaFotoInicialUno.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialDos.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap);
            
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
			
        }else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialDos);
                btnFotoInicialDos.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        	//Cambio CHECA AQUI
        else if (requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoFinalUno.setVisibility(View.VISIBLE);
                vistaFotoFinalUno.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if(requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalUno);
                btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoFinalUno.setVisibility(View.VISIBLE);
                vistaFotoFinalUno.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoFinalDos.setVisibility(View.GONE);
                vistaFotoFinalDos.setVisibility(View.VISIBLE);
                vistaFotoFinalDos.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalDos);
                btnFotoFinalDos.setVisibility(View.GONE);
                vistaFotoFinalDos.setVisibility(View.VISIBLE);
                vistaFotoFinalDos.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    
    
    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;
        
        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;
        
        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }
    
    @Override
    public void onMapClick(LatLng latLng) {
        Toast.makeText(getContext(), "Lati" + latLng.latitude, Toast.LENGTH_SHORT).show();
    }
   
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void cargaDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_folio, null);

        builder.setView(viewDialog);

        final EditText etFolio = viewDialog.findViewById(R.id.etFolio);
        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarFolio);
        Button btnAceptar = viewDialog.findViewById(R.id.btnAceptarFolio);

        final AlertDialog dialog = builder.create();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 folio = etFolio.getText().toString();
                if (!folio.isEmpty()){
                    tvFolio.setText(tvFolio.getText().toString()+folio);
                    dialog.dismiss();

                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void generarTarjetasActividades(int numActividades) {
        recyclerActividadesExtras.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorActividades adapter;
        List<Actividad> ACTIVIDADES = new ArrayList<>();

        for (int i = 0; i < numActividades; i++) {
            String descripcion ="";
            String foto ="";

            ACTIVIDADES.add(new Actividad(descripcion, foto));
            adapter = new AdaptadorActividades(ACTIVIDADES, getActivity());
            recyclerActividadesExtras.setAdapter(adapter);

        }

    }




}
