package com.digital.reportes.ReportesFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.digital.reportes.DigitalActivity;
import com.digital.reportes.LoginActivity;
import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import com.digital.reportes.adaptadores.AdaptadorOrdenesServicio;
import com.digital.reportes.objetos.Constantes;
import com.digital.reportes.objetos.OrdenesServicioPOJO;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ReporteListadoSupervisorFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    private List<OrdenesServicioPOJO> listaOrdenes;
    private AdaptadorOrdenesServicio adaptadorOrdenesServicio;

    private LinearLayout lnLoading, lnContenido;

    private SwipeRefreshLayout swipeRefreshLayout;

    private String clave, evidencia_incial_1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reporte_listado_supervisor, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_listado_supervisor);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Esto se ejecuta cada vez que se realiza el gesto
                listaOrdenes.clear();
                obtenerDatos();
                swipeRefreshLayout.setRefreshing(false);
            }

        });

        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());

        //linearLayoutManager.setReverseLayout(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);


        listaOrdenes = new ArrayList<>();
        //listaOrdenes.clear();

        adaptadorOrdenesServicio = new AdaptadorOrdenesServicio(listaOrdenes, getActivity());
        recyclerView.setAdapter(adaptadorOrdenesServicio);

        lnLoading = view.findViewById(R.id.lnLoading);

        lnContenido = view.findViewById(R.id.lnContenido);
        listaOrdenes.clear();

        if (LoginActivity.tipo_usuario.equals("1")){//1 para Digital
            obtenerDatosDigital();
        }else if(LoginActivity.tipo_usuario.equals("0")){            //0 para supervisor
            obtenerDatos();
        }
        return view;
    }


    private void agregarOrdenes(String os_clave, String os_nombre, String os_fechaIngreso,
                                String os_fechaProgramada, String os_fechaFinalizacion,
                                String os_fechaReprogramacion, String os_status, String os_fkasi,
                                String os_prioridad, String os_observaciones)
    {

        OrdenesServicioPOJO ordenesServicioPOJO = new OrdenesServicioPOJO();
        ordenesServicioPOJO.setOs_clave(os_clave);
        ordenesServicioPOJO.setOs_nombre(os_nombre);
        ordenesServicioPOJO.setOs_fechaIngreso(os_fechaIngreso);
        ordenesServicioPOJO.setOs_fechaProgramada(os_fechaProgramada);
        ordenesServicioPOJO.setOs_fechaFinalizacion(os_fechaFinalizacion);
        ordenesServicioPOJO.setOs_fechaReprogramacion(os_fechaReprogramacion);
        ordenesServicioPOJO.setOs_status(os_status);
        ordenesServicioPOJO.setOs_fkasi(os_fkasi);
        ordenesServicioPOJO.setOs_noOrden("");
        ordenesServicioPOJO.setOs_observaciones(os_observaciones);
        ordenesServicioPOJO.setOs_prioridad(os_prioridad);
        listaOrdenes.add(ordenesServicioPOJO);
        adaptadorOrdenesServicio.notifyDataSetChanged();
    }


    /*private void agregarOrdenes(String os_clave, String os_nombre, String os_fechaIngreso,
                                String os_fechaProgramada, String os_fechaFinalizacion,
                                String os_fechaReprogramacion, String os_status, String os_fkasi,
                                String os_noOrden)
    {

        OrdenesServicioPOJO ordenesServicioPOJO = new OrdenesServicioPOJO();
        ordenesServicioPOJO.setOs_clave(os_clave);
        ordenesServicioPOJO.setOs_nombre(os_nombre);
        ordenesServicioPOJO.setOs_fechaIngreso(os_fechaIngreso);
        ordenesServicioPOJO.setOs_fechaProgramada(os_fechaProgramada);
        ordenesServicioPOJO.setOs_fechaFinalizacion(os_fechaFinalizacion);
        ordenesServicioPOJO.setOs_fechaReprogramacion(os_fechaReprogramacion);
        ordenesServicioPOJO.setOs_status(os_status);
        ordenesServicioPOJO.setOs_fkasi(os_fkasi);
        ordenesServicioPOJO.setOs_noOrden(os_noOrden);
        //ordenesServicioPOJO.setOs_observaciones(os_observaciones);
        //ordenesServicioPOJO.setOs_prioridad(os_prioridad);
        listaOrdenes.add(ordenesServicioPOJO);
        adaptadorOrdenesServicio.notifyDataSetChanged();
    }*/


    private void obtenerDatos() {
        lnContenido.setVisibility(View.GONE);
        lnLoading.setVisibility(View.VISIBLE);
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"ordenes_x_supervisor.php?fksupervirsor="+ LoginActivity.id;
        client.get(url,null,new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("ERror", error.getLocalizedMessage());
            }
        });
    }

    private void obtenerDatosDigital() {
        lnContenido.setVisibility(View.GONE);
        lnLoading.setVisibility(View.VISIBLE);
        AsyncHttpClient client = new AsyncHttpClient();
        //String url = Constantes.URL_PRINCIPAL +"ordenes_x_supervisor.php?fksupervirsor="+ MainActivity.id;
        String url = Constantes.URL_PRINCIPAL +"mostrar_ordenes_servicios.php";

        client.get(url,null,new AsyncHttpResponseHandler(){



            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("ERror", error.getLocalizedMessage());
            }
        });
    }
    private void obtieneJSON(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);

                String clave                       = js.getString("os_clave");
                String nombre               = js.getString("os_nombre");
                String fecha_ingreso        = js.getString("os_fechaIngreso");
                String fecha_programada     = js.getString("os_fechaProgramada");
                String fecha_finalizacion   = js.getString("os_fechaFinalizacion");
                String fecha_reprogramacion = js.getString("os_fechaReprogramacion");
                String status               = js.getString("os_estatus");
                String fkAsignado           = js.getString("os_fkasi");
               // String orden                = js.getString("os_noOrden");
                String observaciones        = js.getString("os_observaciones");
                String prioridad            = js.getString("os_prioridad");

                agregarOrdenes(clave,nombre,fecha_ingreso,fecha_programada,
                        fecha_finalizacion,fecha_reprogramacion,status,fkAsignado,
                        prioridad, observaciones);



                /*

                agregarOrdenes(clave,nombre,fecha_ingreso,fecha_programada,
                        fecha_finalizacion,fecha_reprogramacion,status,fkAsignado,
                        observaciones,prioridad);*/

                lnLoading.setVisibility(View.GONE);
                lnContenido.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            Log.e("ERror", e.getLocalizedMessage());

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
