package com.digital.reportes.ReportesFragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.digital.reportes.BuildConfig;
import com.digital.reportes.LoginActivity;
import com.digital.reportes.MainActivity;
import com.digital.reportes.MapsActivity;
import com.digital.reportes.R;
import com.digital.reportes.adaptadores.AdaptadorActividades;
import com.digital.reportes.objetos.Actividad;
import com.digital.reportes.objetos.Constantes;
import com.digital.reportes.objetos.Reporte;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class ProbandoFragment extends Fragment {
    //fotos
    private FirebaseStorage storage;
    private StorageReference storageReference;
    ProgressDialog progressDialog;
    RequestQueue requestQueue;
    StringRequest stringRequest;


    private OnFragmentInteractionListener mListener;
    CardView card1, card2, card3, card4, card5;
    TextView tvUbicacionActual, tvNoOrden, tvNoOrdenCard1, tvNoOrdenCard2, tvNoOrdenCard3, tvNoOrdenCard4, tvNoOrdenCard5;

    LinearLayout ln1, ln2, ln3, ln4,ln5, ln6, ln7, ln8,
            lnEjecucion, lnNoEjecucion, lnSuccess,
            lnContenedorActividadesExtra, linearMapaContenedor;


    Button btnContinuarEjecucion, btnEnviarNoejecucion, btnContinuarMapa, btnSiguienteFotoInicial, btnSiguienteActividadesRealizar,
            btnSiguienteTareasAdicionales, guardarFirmaBoton,limpiarFirmaBoton, btnEnviarReporteFinal;

    SignaturePad cuadroFirmaRep;

    Switch swOrdenEjecucion, swTareasAdicionales;

    EditText cajaNumActExt;

    RecyclerView recyclerActividadesExtras;
    Button btnCargarDesdeMapa;

    ImageButton btnFotoInicialUno,btnFotoInicialDos,btnFotoFinalUno,btnFotoFinalDos;
    ImageView vistaFotoInicialUno,vistaFotoInicialDos,vistaFotoFinalUno,vistaFotoFinalDos;

    ImageButton btnFotoInicialUnoINE,btnFotoInicialDosINE;
    ImageView vistaFotoInicialUnoINE,vistaFotoInicialDosINE;

    private SupportMapFragment mapFrag;
    private GoogleMap map;

    private static final int SOLICITUD_FOTO_INICIAL_1 = 10;
    private static final int SOLICITUD_FOTO_INICIAL_2 = 11;
    private static final int SOLICITUD_FOTO_FINAL_1 = 12;
    private static final int SOLICITUD_FOTO_FINAL_2 = 13;
    private static final int SOLICITUD_INE_ANVERSO = 15;
    private static final int SOLICITUD_INE_REVERSO = 16;


    private static final int SOLICITUD_FIRMA = 150;


    private Uri uriImagenIncialUno;
    private Uri uriImagenIncialDos;
    private Uri uriImagenFinalUno;
    private Uri uriImagenFinalDos;
    private Uri uriINEanverso;
    private Uri uriINEreverso;
    boolean minizaVista=false;

    int casoTipoFotoSeleccion;


    Geocoder geocoder;
    List<Address> addresses;
    Double latitud = null;
    Double longitud= null;
    private String ubicacion;

    public ProbandoFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_probando, container, false);
        progressDialog = new ProgressDialog(getActivity());


        initComponents(view);
        clicks();
        lastKnowLocation();

        loadMap();



        return view;
    }

    private void loadMap() {
        GoogleMapOptions options = new GoogleMapOptions();
        options.zOrderOnTop(true);

        mapFrag =  (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapillaNuevoReporte);

        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        map.clear();
                        map.addMarker(new MarkerOptions().position(latLng).title("Mi ubicación actual"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(latLng, 17);
                        map.animateCamera(animacionZoom);
                        Constantes.OBJETO_REPORTE.setLatitud(String.valueOf(latLng.latitude));
                        Constantes.OBJETO_REPORTE.setLongitud(String.valueOf(latLng.longitude));

                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try
                        {
                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 100);
                            String direccion = addresses.get(0).getAddressLine(0);
                            String codigopostal= addresses.get(0).getPostalCode();
                            String   direccionActual = direccion+", "+codigopostal;
                            Constantes.OBJETO_REPORTE.setDireccion(direccionActual);

                        }catch (Exception e){
                            Toast.makeText(getContext(), "Ha ocurrido un problema a la hora de obtener la dirección actual, por favor intente nuevamente...", Toast.LENGTH_LONG).show();

                        }

                    }
                });


            }
        });
    }

    private void initComponents(View view) {
        requestQueue = Volley.newRequestQueue(getActivity());


        //fotos
        storage = FirebaseStorage.getInstance();


        card1 = view.findViewById(R.id.card1);
        tvNoOrdenCard1 = view.findViewById(R.id.tvNoOrdenCard1);
        card2 = view.findViewById(R.id.card2);
        tvNoOrdenCard2 = view.findViewById(R.id.tvNoOrdenCard2);
        card3 = view.findViewById(R.id.card3);
        tvNoOrdenCard3 = view.findViewById(R.id.tvNoOrdenCard3);
        card4 = view.findViewById(R.id.card4);
        tvNoOrdenCard4 = view.findViewById(R.id.tvNoOrdenCard4);
        card5 = view.findViewById(R.id.card5);
        tvNoOrdenCard5 = view.findViewById(R.id.tvNoOrdenCard5);

        ln1 = view.findViewById(R.id.ln1);
        ln2 = view.findViewById(R.id.ln2);
        ln3 = view.findViewById(R.id.ln3);
        ln4 = view.findViewById(R.id.ln4);
        ln5 = view.findViewById(R.id.ln5);
        ln6 = view.findViewById(R.id.ln6);
        ln7 = view.findViewById(R.id.ln7);
        ln8 = view.findViewById(R.id.ln8);
        swOrdenEjecucion = view.findViewById(R.id.swOrdenEjecucion);

        lnEjecucion       = view.findViewById(R.id.lnEjecucion);
        lnNoEjecucion     = view.findViewById(R.id.lnNoEjecucion);

        btnContinuarEjecucion = view.findViewById(R.id.btnContinuarEjecucion);
        btnEnviarNoejecucion  = view.findViewById(R.id.btnEnviarNoejecucion);

        btnContinuarMapa = view.findViewById(R.id.btnContinuarMapa);
        btnSiguienteFotoInicial = view.findViewById(R.id.btnSiguienteFotoInicial);
        btnSiguienteActividadesRealizar = view.findViewById(R.id.btnSiguienteActividadesRealizar);

        cuadroFirmaRep          =     view.findViewById(R.id.firmaCuadroReporte);
        guardarFirmaBoton = view.findViewById(R.id.guardarFirmaBoton);
        limpiarFirmaBoton = view.findViewById(R.id.limpiarFirmaBoton);
        lnSuccess = view.findViewById(R.id.lnSuccess);

        swTareasAdicionales = view.findViewById(R.id.swTareasAdicionales);
        lnContenedorActividadesExtra = view.findViewById(R.id.lnContenedorActividadesExtra);
        btnSiguienteTareasAdicionales = view.findViewById(R.id.btnSiguienteTareasAdicionales);

        btnEnviarReporteFinal = view.findViewById(R.id.btnEnviarReporteFinal);

        tvUbicacionActual = view.findViewById(R.id.tvUbicacionActual);
        tvNoOrden         = view.findViewById(R.id.tvNoOrden);


        //Funcinalidades
        linearMapaContenedor = view.findViewById(R.id.linearContenedorMapilla);
        recyclerActividadesExtras = view.findViewById(R.id.recyclerActividadesExtras);
        btnCargarDesdeMapa = view.findViewById(R.id.btnCargarUbicacionMapa);

        btnFotoInicialUno = view.findViewById(R.id.imgButtonFotoInicialUno);
        vistaFotoInicialUno = view.findViewById(R.id.imgFotoInicialUnoVista);
        btnFotoInicialDos= view.findViewById(R.id.imgButtonFotoInicialDos);
        vistaFotoInicialDos = view.findViewById(R.id.imgFotoInicialDosVista);

        btnFotoFinalUno      = view.findViewById(R.id.imgButtonFotoFinalUno);
        btnFotoFinalDos      = view.findViewById(R.id.imgButtonFotoFinalDos);
        vistaFotoFinalUno    = view.findViewById(R.id.imgFotoFinalUnoVista);
        vistaFotoFinalDos    = view.findViewById(R.id.imgFotoFinalDosVista);

        cajaNumActExt        = view.findViewById(R.id.cajaNumActividadesExtras);

        btnFotoInicialUnoINE     = view.findViewById(R.id.imgButtonFotoInicialUnoINE);
        vistaFotoInicialUnoINE   = view.findViewById(R.id.imgFotoInicialUnoVistaINE);
        btnFotoInicialDosINE     = view.findViewById(R.id.imgButtonFotoInicialDosINE);
        vistaFotoInicialDosINE   = view.findViewById(R.id.imgFotoInicialDosVistaINE);
    }
    private void clicks() {
        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln1.setVisibility(View.GONE);
                tvNoOrden.setVisibility(View.VISIBLE);
                tvNoOrden.setText("Orden: "+tvNoOrdenCard1.getText().toString());
                ln2.setVisibility(View.VISIBLE);

                Constantes.OBJETO_REPORTE.setUsuario("1");
                Constantes.OBJETO_REPORTE.setFolio(tvNoOrdenCard1.getText().toString());

            }
        });
        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln1.setVisibility(View.GONE);
                tvNoOrden.setVisibility(View.VISIBLE);
                tvNoOrden.setText("Orden: "+tvNoOrdenCard2.getText().toString());
                ln2.setVisibility(View.VISIBLE);

            }
        });
        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln1.setVisibility(View.GONE);
                tvNoOrden.setVisibility(View.VISIBLE);
                tvNoOrden.setText("Orden: "+tvNoOrdenCard3.getText().toString());
                ln2.setVisibility(View.VISIBLE);

            }
        });
        card4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln1.setVisibility(View.GONE);
                tvNoOrden.setVisibility(View.VISIBLE);
                tvNoOrden.setText("Orden: "+tvNoOrdenCard4.getText().toString());
                ln2.setVisibility(View.VISIBLE);

            }
        });
        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln1.setVisibility(View.GONE);
                tvNoOrden.setVisibility(View.VISIBLE);

                tvNoOrden.setText("Orden: "+tvNoOrdenCard5.getText().toString());
                ln2.setVisibility(View.VISIBLE);

            }
        });

        swOrdenEjecucion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    lnNoEjecucion.setVisibility(View.GONE);
                    lnEjecucion.setVisibility(View.VISIBLE);

                }else{
                    lnEjecucion.setVisibility(View.GONE);
                    lnNoEjecucion.setVisibility(View.VISIBLE);
                }
            }
        });

        swTareasAdicionales.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    lnContenedorActividadesExtra.setVisibility(View.VISIBLE);
                }else{
                    lnContenedorActividadesExtra.setVisibility(View.GONE);

                }
            }
        });

        btnEnviarNoejecucion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvNoOrden.setVisibility(View.GONE);
                ln1.setVisibility(View.GONE);
                ln2.setVisibility(View.GONE);
                lnSuccess.setVisibility(View.VISIBLE);
            }
        });
        btnContinuarEjecucion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln2.setVisibility(View.GONE);
                ln3.setVisibility(View.VISIBLE);
            }
        });
        btnContinuarMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), Constantes.OBJETO_REPORTE.getDireccion() , Toast.LENGTH_LONG).show();
                ln3.setVisibility(View.GONE);
                ln4.setVisibility(View.VISIBLE);
            }
        });
        btnSiguienteFotoInicial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln4.setVisibility(View.GONE);
                ln5.setVisibility(View.VISIBLE);
            }
        });
        btnSiguienteActividadesRealizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln5.setVisibility(View.GONE);
                ln6.setVisibility(View.VISIBLE);

            }
        });
        btnSiguienteTareasAdicionales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln6.setVisibility(View.GONE);
                ln7.setVisibility(View.VISIBLE);
            }
        });
        guardarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{

                    subirFotoFirebase(bitmapToUri(getContext(),cuadroFirmaRep.getSignatureBitmap()),SOLICITUD_FIRMA);


                }catch (Exception e){
                    Toast.makeText(getContext(), "Se produjo un problema a intentar subir, intente nuevamente", Toast.LENGTH_LONG).show();
                }





            }
        });

        limpiarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cuadroFirmaRep.clear();

            }
        });

        btnEnviarReporteFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Usuario: " + Constantes.OBJETO_REPORTE.getUsuario());
                System.out.println("Folio: " + Constantes.OBJETO_REPORTE.getFolio());
                System.out.println("Dirección: " + Constantes.OBJETO_REPORTE.getDireccion());
                System.out.println("Latitud: " + Constantes.OBJETO_REPORTE.getLatitud());
                System.out.println("Longitud: " + Constantes.OBJETO_REPORTE.getLatitud());
                System.out.println("Foto evidencia inicial uno: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_uno());
                System.out.println("Foto evidencia inicial dos: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_dos());
                System.out.println("Foto evidencia final uno: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_final_uno());
                System.out.println("Foto evidencia final dos: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_final_dos());
                System.out.println("Firma VOBO: " + Constantes.OBJETO_REPORTE.getFirmavobo());
                System.out.println("Foto ine anverso: " + Constantes.OBJETO_REPORTE.getFoto_ine_anverso());
                System.out.println("Foto ine reverso: " + Constantes.OBJETO_REPORTE.getFoto_ine_reverso());

                try{
                enviarRegistroBD(Constantes.OBJETO_REPORTE);
                ln8.setVisibility(View.GONE);
                tvNoOrden.setVisibility(View.GONE);
                lnSuccess.setVisibility(View.VISIBLE);

                }catch (Exception e){
                    Toast.makeText(getContext(), "Se produjo un problema a intentar subir, intente nuevamente", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnCargarDesdeMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // startActivity(new Intent(getContext(), MapsActivity.class));
                Intent intent = new Intent(getContext(), MapsActivity.class);
                intent.putExtra("latitudExtra", String.valueOf(latitud));
                intent.putExtra("longitudExtra", String.valueOf(longitud));

                startActivity(intent);
               // linearMapaContenedor.setVisibility(View.VISIBLE);
            }
        });

        //FOTOS CLICKS

        btnFotoInicialUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(1);
            }
        });



        btnFotoInicialDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(2);
            }
        });

        btnFotoFinalUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(3);
            }
        });
        btnFotoFinalDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(4);
            }
        });

        cajaNumActExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (cajaNumActExt.getText().toString() != null ) {
                        int numActividades = Integer.parseInt(cajaNumActExt.getText().toString());
                        generarTarjetasActividades(numActividades);

                    }else if( cajaNumActExt.getText().toString() == ""){
                        generarTarjetasActividades(0);

                    }

                }catch (Exception e) {
                    Log.e("ERROR", e.getLocalizedMessage());
                }

            }
        });

        btnFotoInicialUnoINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(5);
            }

        });

        btnFotoInicialDosINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(6);
            }
        });

    }


    //ANTES DE ENVIAR ASEGURARSE DE PASAR AL OBJETO REPORTE "setOrden"
    private void enviarRegistroBD(Reporte objetoReporte) {
        String url= Constantes.URL_PRINCIPAL +"insertar_reporte.php?rep_folio="+Constantes.OBJETO_REPORTE.getFolio()+"&rep_direccion="+Constantes.OBJETO_REPORTE.getDireccion()+"&rep_latitud="+Constantes.OBJETO_REPORTE.getLatitud()+"&rep_longitud="+Constantes.OBJETO_REPORTE.getLongitud()+"&rep_evidencia_inicial_uno="+Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_uno()+"&rep_evidencia_inicial_dos="+Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_dos()+"&rep_evidencia_final_uno="+Constantes.OBJETO_REPORTE.getFoto_evidencia_final_uno()+"&rep_evidencia_final_dos="+Constantes.OBJETO_REPORTE.getFoto_evidencia_final_dos()+"&rep_firmavobo="+Constantes.OBJETO_REPORTE.getFirmavobo()+"&rep_ine_anverso="+Constantes.OBJETO_REPORTE.getFoto_ine_anverso()+"&rep_ine_reverso="+Constantes.OBJETO_REPORTE.getFoto_ine_reverso()+"&fkorden="+Constantes.OBJETO_REPORTE.getOrden();
        System.out.println("URL A EJECUTAR: " +url);
        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getContext(), "Reporte realizado!!", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Se produjo un problema a la hora de realizar el registro. "+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(stringRequest);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void lastKnowLocation()
    {
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(getActivity());
        client.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>()
        {
            @Override
            public void onSuccess(Location location)
            {
                if (location != null)
                {
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();

                    gpsData(latitud, longitud);
                }
            }
        });

    }
    private void gpsData(Double latitud, Double longitud)
    {
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try
        {
            addresses = geocoder.getFromLocation(latitud, longitud, 100);

            String direccion = addresses.get(0).getAddressLine(0);
            //String area      = addresses.get(0).getLocality();
            String ciudad    = addresses.get(0).getAdminArea();
            String codigopostal= addresses.get(0).getPostalCode();
            //latitud = addresses.get(0).getLatitude();
            //longitud= addresses.get(0).getLongitude();


            ubicacion = direccion+", "+codigopostal;

            tvUbicacionActual.setText(tvUbicacionActual.getText().toString()+ubicacion);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal, fragment);
        ft.commit();
    }

    public Uri bitmapToUri(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "firma_vobo", null);
        return Uri.parse(path);
    }

    private void obtenerFoto(final int num_foto) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);

        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);


        final AlertDialog dialog = builder.create();

        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=1;

                verificarPermisosCamaraMemoria(num_foto, 1);
                dialog.dismiss();


            }
        });

        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=2;

                verificarPermisosCamaraMemoria(num_foto, 2);

                dialog.dismiss();

            }
        });



        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);

            }
        }


    }

    private void abrirCamara(int foto, int tipoCaso) {

        if(foto==1 && tipoCaso==1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenIncialUno);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_INICIAL_1);

        }else if(foto==2 && tipoCaso==1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenIncialDos);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_INICIAL_2);

        }else if(foto==3 && tipoCaso==1){

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenFinalUno);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_FINAL_1);

        }else if(foto==4 && tipoCaso==1){

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenFinalDos);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_FINAL_2);

        }else  if(foto==1 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_uno"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenIncialUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialUno);
                startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_1);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }else  if(foto==2 && tipoCaso==2){

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_dos"+currentDateandTime+".jpg");
            if (getActivity() != null){
                uriImagenIncialDos = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialDos);
                startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_2);
            }

        }else if(foto==3 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_uno"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenFinalUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalUno);
                startActivityForResult(intent, SOLICITUD_FOTO_FINAL_1);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }else if(foto==4 && tipoCaso == 2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_dos"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenFinalDos = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalDos);
                startActivityForResult(intent, SOLICITUD_FOTO_FINAL_2);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }


        }else if(foto==5 && tipoCaso == 1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriINEanverso);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_INE_ANVERSO);

        }else if(foto==6 && tipoCaso == 1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriINEreverso);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_INE_REVERSO);

        }else if(foto==5 && tipoCaso == 2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "foto_ine_anverso"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriINEanverso = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriINEanverso);
                startActivityForResult(intent, SOLICITUD_INE_ANVERSO);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }

        }else if(foto==6 && tipoCaso == 2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "foto_ine_reverso"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriINEreverso = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriINEreverso);
                startActivityForResult(intent, SOLICITUD_INE_REVERSO);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }



        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA

        if (requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_FOTO_INICIAL_1);



            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if(requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {

                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialUno);
                btnFotoInicialUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenIncialUno,SOLICITUD_FOTO_INICIAL_1);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialDos.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_FOTO_INICIAL_2);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialDos);
                btnFotoInicialDos.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenIncialDos,SOLICITUD_FOTO_INICIAL_2);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        //Cambio CHECA AQUI
        else if (requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoFinalUno.setVisibility(View.VISIBLE);
                vistaFotoFinalUno.setImageBitmap(bitmap);


                subirFotoFirebase(u,SOLICITUD_FOTO_FINAL_1);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if(requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalUno);
                btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoFinalUno.setVisibility(View.VISIBLE);
                vistaFotoFinalUno.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenFinalUno,SOLICITUD_FOTO_FINAL_1);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoFinalDos.setVisibility(View.GONE);
                vistaFotoFinalDos.setVisibility(View.VISIBLE);
                vistaFotoFinalDos.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_FOTO_FINAL_2);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalDos);
                btnFotoFinalDos.setVisibility(View.GONE);
                vistaFotoFinalDos.setVisibility(View.VISIBLE);
                vistaFotoFinalDos.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenFinalDos,SOLICITUD_FOTO_FINAL_2);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_INE_ANVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialUnoINE.setVisibility(View.GONE);
                vistaFotoInicialUnoINE.setVisibility(View.VISIBLE);
                vistaFotoInicialUnoINE.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_INE_ANVERSO);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_INE_ANVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriINEanverso);
                btnFotoInicialUnoINE.setVisibility(View.GONE);
                vistaFotoInicialUnoINE.setVisibility(View.VISIBLE);
                vistaFotoInicialUnoINE.setImageBitmap(bitmap);

                subirFotoFirebase(uriINEanverso,SOLICITUD_INE_ANVERSO);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_INE_REVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialDosINE.setVisibility(View.GONE);
                vistaFotoInicialDosINE.setVisibility(View.VISIBLE);
                vistaFotoInicialDosINE.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_INE_REVERSO);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_INE_REVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriINEreverso);
                btnFotoInicialDosINE.setVisibility(View.GONE);
                vistaFotoInicialDosINE.setVisibility(View.VISIBLE);
                vistaFotoInicialDosINE.setImageBitmap(bitmap);
                minizaVista=true;
                subirFotoFirebase(uriINEreverso,SOLICITUD_INE_REVERSO);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


    }


    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }
    private void generarTarjetasActividades(int numActividades) {
        recyclerActividadesExtras.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorActividades adapter;
        List<Actividad> ACTIVIDADES = new ArrayList<>();

        for (int i = 0; i < numActividades; i++) {
            String descripcion ="";
            String foto ="";

            ACTIVIDADES.add(new Actividad(descripcion, foto));
            adapter = new AdaptadorActividades(ACTIVIDADES, getActivity());
            recyclerActividadesExtras.setAdapter(adapter);

        }

    }

    public void onMapClick(LatLng latLng) {
        Toast.makeText(getContext(), "Lati" + latLng.latitude, Toast.LENGTH_SHORT).show();
    }


    void subirFotoFirebase(Uri uri, final int numFoto){


        storageReference = storage.getReference("reporte_"+Constantes.OBJETO_REPORTE.getFolio());//FOTOS SEGÚN SEA EL CASO

        switch (numFoto){
            case SOLICITUD_FOTO_INICIAL_1:

                procesoSubidaFirebase(uri,SOLICITUD_FOTO_INICIAL_1);

                break;
            case SOLICITUD_FOTO_INICIAL_2:

                procesoSubidaFirebase(uri,SOLICITUD_FOTO_INICIAL_2);

                break;

            case SOLICITUD_FOTO_FINAL_1:

                procesoSubidaFirebase(uri,SOLICITUD_FOTO_FINAL_1);

                break;
            case SOLICITUD_FOTO_FINAL_2:

                procesoSubidaFirebase(uri,SOLICITUD_FOTO_FINAL_2);


                break;
            case SOLICITUD_FIRMA:

                procesoSubidaFirebase(uri,SOLICITUD_FIRMA);


                break;

            case SOLICITUD_INE_ANVERSO:

                procesoSubidaFirebase(uri,SOLICITUD_INE_ANVERSO);


                break;

            case SOLICITUD_INE_REVERSO:

                procesoSubidaFirebase(uri,SOLICITUD_INE_REVERSO);

            if(Constantes.OBJETO_REPORTE.getFirmavobo().isEmpty()==false
            && Constantes.OBJETO_REPORTE.getFoto_ine_anverso().isEmpty()==false
            && Constantes.OBJETO_REPORTE.getFoto_ine_reverso().isEmpty()==false)
            {
                ln7.setVisibility(View.GONE);
                ln8.setVisibility(View.VISIBLE);
            }


                break;

        }





    }

    private void procesoSubidaFirebase(Uri uri, final int numFoto) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());
        progressDialog.setTitle("Subiendo tu foto");
        progressDialog.setMessage("Subiendo foto");
        progressDialog.setCancelable(false);
        progressDialog.show();
        UploadTask uploadTask = fotoReferencia.putFile(uri);


        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal=task.getResult().toString();

                switch (numFoto){
                    case SOLICITUD_FOTO_INICIAL_1:
                        Constantes.OBJETO_REPORTE.setFoto_evidencia_inicial_uno(uriFinal);


                        break;
                    case SOLICITUD_FOTO_INICIAL_2:
                        Constantes.OBJETO_REPORTE.setFoto_evidencia_inicial_dos(uriFinal);


                        break;

                    case SOLICITUD_FOTO_FINAL_1:
                        Constantes.OBJETO_REPORTE.setFoto_evidencia_final_uno(uriFinal);


                        break;
                    case SOLICITUD_FOTO_FINAL_2:

                        Constantes.OBJETO_REPORTE.setFoto_evidencia_final_dos(uriFinal);

                        break;
                    case SOLICITUD_FIRMA:

                        Constantes.OBJETO_REPORTE.setFirmavobo(uriFinal);


                        break;

                    case SOLICITUD_INE_ANVERSO:

                        Constantes.OBJETO_REPORTE.setFoto_ine_anverso(uriFinal);


                        break;

                    case SOLICITUD_INE_REVERSO:


                        Constantes.OBJETO_REPORTE.setFoto_ine_reverso(uriFinal);


                        break;

                }


                progressDialog.hide();
                Toast.makeText(getContext(),"Foto cargada!", Toast.LENGTH_LONG).show();


            }
        });
    }


}

