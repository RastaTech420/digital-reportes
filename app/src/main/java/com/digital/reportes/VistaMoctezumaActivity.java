package com.digital.reportes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.digital.reportes.ReportesFragment.DetalleReporteMoctezumaFragment;
import com.digital.reportes.ReportesFragment.ReporteListadoMoctezumaFragment;
import com.digital.reportes.ViewReporteFinal.DetalleReporteFragment;

public class VistaMoctezumaActivity extends AppCompatActivity
implements ReporteListadoMoctezumaFragment.OnFragmentInteractionListener,
        DetalleReporteFragment.OnFragmentInteractionListener,
        DetalleReporteMoctezumaFragment.OnFragmentInteractionListener {
    FrameLayout contenedor_vista_moctezuma;
    public static String id, nombre_usuario, tipo_usuario, direccion_completa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_moctezuma);

        contenedor_vista_moctezuma = findViewById(R.id.contenedor_vista_moctezuma);
        Fragment listadoMoctezuma = new ReporteListadoMoctezumaFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.contenedor_vista_moctezuma, listadoMoctezuma);
        ft.commit();

        datosLogin();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_toolbar) {
            Intent i = new Intent(VistaMoctezumaActivity.this, LoginActivity.class);
            overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    private void datosLogin() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        id             = bundle.getString("id");
        nombre_usuario = bundle.getString("nombre");
        tipo_usuario   = bundle.getString("tipo_usuario");
        setTitle("Bienvenido "+nombre_usuario);
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
