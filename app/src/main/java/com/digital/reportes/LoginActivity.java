package com.digital.reportes;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    private Button btnInciarSesion, btnOlvideContra;
    private EditText etUsuario, etContra;
    private LinearLayout lyErrorPass, lnLoading;
    public static String id, tipo_usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        inicializarElementos();
        clicks();
    }

    private void inicializarElementos() {
        btnInciarSesion = findViewById(R.id.btnIniciarSesion);
        btnOlvideContra = findViewById(R.id.btnOlvideContra);
        etUsuario       = findViewById(R.id.etUsuario);
        etContra        = findViewById(R.id.etContra);
        lyErrorPass     = findViewById(R.id.lyErrorPass);
        lnLoading       = findViewById(R.id.lnLoading);
    }

    private void clicks() {
        btnInciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLoginData();
            }
        });

        btnOlvideContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cargaDialog();
            }
        });
    }

    private void cargaDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_contra, null);

        builder.setView(viewDialog);

        final EditText etIdEmpleado = viewDialog.findViewById(R.id.etIdEmpleadoDialogContra);
        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarContra);
        Button btnRecuperar = viewDialog.findViewById(R.id.btnRecuperarContra);

        final AlertDialog dialog = builder.create();

        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String idempleado = etIdEmpleado.getText().toString();
                Toast.makeText(LoginActivity.this, "Si el ID existe se enviará un vínculo al correo vinculado al ID", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void checkLoginData(){
        btnInciarSesion.setVisibility(View.GONE);
        lnLoading.setVisibility(View.VISIBLE);
        String usuario = etUsuario.getText().toString().toLowerCase();
        String pass = etContra.getText().toString();

        AsyncHttpClient cliente = new AsyncHttpClient();
        String url = "http://www.dc.vigux.com.mx/site/swlogin?user="+usuario+"&pass="+pass;

        cliente.get(url,null, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    comprobarDatos(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Error de conexión...
                //Checar que hacer cuando no hay conexión
                //Con sharedpreferences dejar logueado cuando ya se haya ingresado con conexión
                btnInciarSesion.setVisibility(View.VISIBLE);

            }
        });

    }

    private void comprobarDatos(String response) {
        try{
            JSONObject jsonObject = new JSONObject(response);
            String ids     = jsonObject.getString("id");
            String nombre = jsonObject.getString("nombre");
            String tipo   = jsonObject.getString("tipo");
            tipo_usuario  = tipo;
            id = ids;
            switch (tipo) {
                case "0": {
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.putExtra("id", ids);
                    i.putExtra("nombre", nombre);
                    i.putExtra("tipo_usuario", tipo);
                    overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);

                    startActivity(i);
                    finish();
                    break;
                }
                case "1": {
                    Intent i = new Intent(LoginActivity.this, DigitalActivity.class);
                    i.putExtra("id", ids);
                    i.putExtra("nombre", nombre);
                    i.putExtra("tipo_usuario", tipo);
                    overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);

                    startActivity(i);
                    finish();

                    break;
                }
                case "3": {
                    Intent i = new Intent(LoginActivity.this, VistaMoctezumaActivity.class);
                    i.putExtra("id", ids);
                    i.putExtra("nombre", nombre);
                    i.putExtra("tipo_usuario", tipo);
                    overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);
                    startActivity(i);
                    finish();
                    break;
                }
            }

            } catch (JSONException ex) {
            lyErrorPass.setVisibility(View.VISIBLE);
            btnInciarSesion.setVisibility(View.VISIBLE);
            lnLoading.setVisibility(View.GONE);
            ex.printStackTrace();
        }

    }
}
