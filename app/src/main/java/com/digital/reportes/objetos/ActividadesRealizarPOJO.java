package com.digital.reportes.objetos;

public class ActividadesRealizarPOJO {
    String os_clave, os_nombre, paq_nombre, act_nombre;

    public ActividadesRealizarPOJO(String os_clave, String os_nombre, String paq_nombre, String act_nombre) {
        this.os_clave = os_clave;
        this.os_nombre = os_nombre;
        this.paq_nombre = paq_nombre;
        this.act_nombre = act_nombre;
    }
    public ActividadesRealizarPOJO(){}

    public String getOs_clave() {
        return os_clave;
    }

    public void setOs_clave(String os_clave) {
        this.os_clave = os_clave;
    }

    public String getOs_nombre() {
        return os_nombre;
    }

    public void setOs_nombre(String os_nombre) {
        this.os_nombre = os_nombre;
    }

    public String getPaq_nombre() {
        return paq_nombre;
    }

    public void setPaq_nombre(String paq_nombre) {
        this.paq_nombre = paq_nombre;
    }

    public String getAct_nombre() {
        return act_nombre;
    }

    public void setAct_nombre(String act_nombre) {
        this.act_nombre = act_nombre;
    }
}
