package com.digital.reportes.objetos;

public class ReporteMoctezuma {
    String rep_clave, rep_fkusuario, rep_folio, rep_direccion, rep_latitud, rep_longitud, rep_evidencia_inicial_uno, rep_evidencia_inicial_dos,
            rep_evidencia_final_uno, rep_evidencia_final_dos, rep_firmavobo, rep_ine_anverso, rep_ine_reverso;

    public ReporteMoctezuma(){}

    public ReporteMoctezuma(String rep_clave, String rep_fkusuario, String rep_folio, String rep_direccion, String rep_latitud, String rep_longitud, String rep_evidencia_inicial_uno, String rep_evidencia_inicial_dos, String rep_evidencia_final_uno, String rep_evidencia_final_dos, String rep_firmavobo, String rep_ine_anverso, String rep_ine_reverso) {
        this.rep_clave = rep_clave;
        this.rep_fkusuario = rep_fkusuario;
        this.rep_folio = rep_folio;
        this.rep_direccion = rep_direccion;
        this.rep_latitud = rep_latitud;
        this.rep_longitud = rep_longitud;
        this.rep_evidencia_inicial_uno = rep_evidencia_inicial_uno;
        this.rep_evidencia_inicial_dos = rep_evidencia_inicial_dos;
        this.rep_evidencia_final_uno = rep_evidencia_final_uno;
        this.rep_evidencia_final_dos = rep_evidencia_final_dos;
        this.rep_firmavobo = rep_firmavobo;
        this.rep_ine_anverso = rep_ine_anverso;
        this.rep_ine_reverso = rep_ine_reverso;
    }

    public String getRep_clave() {
        return rep_clave;
    }

    public void setRep_clave(String rep_clave) {
        this.rep_clave = rep_clave;
    }

    public String getRep_fkusuario() {
        return rep_fkusuario;
    }

    public void setRep_fkusuario(String rep_fkusuario) {
        this.rep_fkusuario = rep_fkusuario;
    }

    public String getRep_folio() {
        return rep_folio;
    }

    public void setRep_folio(String rep_folio) {
        this.rep_folio = rep_folio;
    }

    public String getRep_direccion() {
        return rep_direccion;
    }

    public void setRep_direccion(String rep_direccion) {
        this.rep_direccion = rep_direccion;
    }

    public String getRep_latitud() {
        return rep_latitud;
    }

    public void setRep_latitud(String rep_latitud) {
        this.rep_latitud = rep_latitud;
    }

    public String getRep_longitud() {
        return rep_longitud;
    }

    public void setRep_longitud(String rep_longitud) {
        this.rep_longitud = rep_longitud;
    }

    public String getRep_evidencia_inicial_uno() {
        return rep_evidencia_inicial_uno;
    }

    public void setRep_evidencia_inicial_uno(String rep_evidencia_inicial_uno) {
        this.rep_evidencia_inicial_uno = rep_evidencia_inicial_uno;
    }

    public String getRep_evidencia_inicial_dos() {
        return rep_evidencia_inicial_dos;
    }

    public void setRep_evidencia_inicial_dos(String rep_evidencia_inicial_dos) {
        this.rep_evidencia_inicial_dos = rep_evidencia_inicial_dos;
    }

    public String getRep_evidencia_final_uno() {
        return rep_evidencia_final_uno;
    }

    public void setRep_evidencia_final_uno(String rep_evidencia_final_uno) {
        this.rep_evidencia_final_uno = rep_evidencia_final_uno;
    }

    public String getRep_evidencia_final_dos() {
        return rep_evidencia_final_dos;
    }

    public void setRep_evidencia_final_dos(String rep_evidencia_final_dos) {
        this.rep_evidencia_final_dos = rep_evidencia_final_dos;
    }

    public String getRep_firmavobo() {
        return rep_firmavobo;
    }

    public void setRep_firmavobo(String rep_firmavobo) {
        this.rep_firmavobo = rep_firmavobo;
    }

    public String getRep_ine_anverso() {
        return rep_ine_anverso;
    }

    public void setRep_ine_anverso(String rep_ine_anverso) {
        this.rep_ine_anverso = rep_ine_anverso;
    }

    public String getRep_ine_reverso() {
        return rep_ine_reverso;
    }

    public void setRep_ine_reverso(String rep_ine_reverso) {
        this.rep_ine_reverso = rep_ine_reverso;
    }
}
