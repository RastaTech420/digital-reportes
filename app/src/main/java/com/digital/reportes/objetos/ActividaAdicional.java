package com.digital.reportes.objetos;

public class ActividaAdicional {

    String numActividad, descripcion, foto;

    ActividaAdicional(){}

    public ActividaAdicional(String numActividad, String descripcion, String foto) {
        this.numActividad = numActividad;
        this.descripcion = descripcion;
        this.foto = foto;
    }

    public String getNumActividad() {
        return numActividad;
    }

    public void setNumActividad(String numActividad) {
        this.numActividad = numActividad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
