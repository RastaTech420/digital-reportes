package com.digital.reportes.objetos;

public class OrdenesServicioPOJO {
    private String os_clave, os_nombre, os_fechaIngreso,os_fechaProgramada, os_fechaFinalizacion, os_fechaReprogramacion,
    os_status, os_fkasi, os_noOrden, os_observaciones, os_prioridad;

    public OrdenesServicioPOJO(String os_clave, String os_nombre, String os_fechaIngreso, String os_fechaProgramada, String os_fechaFinalizacion, String os_fechaReprogramacion, String os_status, String os_fkasi, String os_noOrden, String os_observaciones, String os_prioridad) {
        this.os_clave = os_clave;
        this.os_nombre = os_nombre;
        this.os_fechaIngreso = os_fechaIngreso;
        this.os_fechaProgramada = os_fechaProgramada;
        this.os_fechaFinalizacion = os_fechaFinalizacion;
        this.os_fechaReprogramacion = os_fechaReprogramacion;
        this.os_status = os_status;
        this.os_fkasi = os_fkasi;
        this.os_noOrden = os_noOrden;
        this.os_observaciones = os_observaciones;
        this.os_prioridad = os_prioridad;
    }
    public OrdenesServicioPOJO(){}

    public String getOs_clave() {
        return os_clave;
    }

    public void setOs_clave(String os_clave) {
        this.os_clave = os_clave;
    }

    public String getOs_nombre() {
        return os_nombre;
    }

    public void setOs_nombre(String os_nombre) {
        this.os_nombre = os_nombre;
    }

    public String getOs_fechaIngreso() {
        return os_fechaIngreso;
    }

    public void setOs_fechaIngreso(String os_fechaIngreso) {
        this.os_fechaIngreso = os_fechaIngreso;
    }

    public String getOs_fechaProgramada() {
        return os_fechaProgramada;
    }

    public void setOs_fechaProgramada(String os_fechaProgramada) {
        this.os_fechaProgramada = os_fechaProgramada;
    }

    public String getOs_fechaFinalizacion() {
        return os_fechaFinalizacion;
    }

    public void setOs_fechaFinalizacion(String os_fechaFinalizacion) {
        this.os_fechaFinalizacion = os_fechaFinalizacion;
    }

    public String getOs_fechaReprogramacion() {
        return os_fechaReprogramacion;
    }

    public void setOs_fechaReprogramacion(String os_fechaReprogramacion) {
        this.os_fechaReprogramacion = os_fechaReprogramacion;
    }

    public String getOs_status() {
        return os_status;
    }

    public void setOs_status(String os_status) {
        this.os_status = os_status;
    }

    public String getOs_fkasi() {
        return os_fkasi;
    }

    public void setOs_fkasi(String os_fkasi) {
        this.os_fkasi = os_fkasi;
    }

    public String getOs_noOrden() {
        return os_noOrden;
    }

    public void setOs_noOrden(String os_noOrden) {
        this.os_noOrden = os_noOrden;
    }

    public String getOs_observaciones() {
        return os_observaciones;
    }

    public void setOs_observaciones(String os_observaciones) {
        this.os_observaciones = os_observaciones;
    }

    public String getOs_prioridad() {
        return os_prioridad;
    }

    public void setOs_prioridad(String os_prioridad) {
        this.os_prioridad = os_prioridad;
    }
}
