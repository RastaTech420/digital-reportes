package com.digital.reportes.objetos;

public class Reporte {
    String clave;
    String usuario;
    String folio;
    String direccion;
    String latitud;
    String longitud;
    String foto_evidencia_inicial_uno;
    String foto_evidencia_inicial_dos;
    String foto_evidencia_final_uno;
    String foto_evidencia_final_dos;
    String firmavobo;
    String foto_ine_anverso;
    String foto_ine_reverso;
    String orden;

    public Reporte(String clave, String usuario, String folio, String direccion, String latitud, String longitud, String foto_evidencia_inicial_uno, String foto_evidencia_inicial_dos, String foto_evidencia_final_uno, String foto_evidencia_final_dos, String firmavobo, String foto_ine_anverso, String foto_ine_reverso, String orden) {
        this.clave = clave;
        this.usuario = usuario;
        this.folio = folio;
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
        this.foto_evidencia_inicial_uno = foto_evidencia_inicial_uno;
        this.foto_evidencia_inicial_dos = foto_evidencia_inicial_dos;
        this.foto_evidencia_final_uno = foto_evidencia_final_uno;
        this.foto_evidencia_final_dos = foto_evidencia_final_dos;
        this.firmavobo = firmavobo;
        this.foto_ine_anverso = foto_ine_anverso;
        this.foto_ine_reverso = foto_ine_reverso;
        this.orden = orden;
    }


    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFoto_evidencia_inicial_uno() {
        return foto_evidencia_inicial_uno.replace("%", "%25").replace("&", "%26");
    }

    public void setFoto_evidencia_inicial_uno(String foto_evidencia_inicial_uno) {
        this.foto_evidencia_inicial_uno = foto_evidencia_inicial_uno;
    }

    public String getFoto_evidencia_inicial_dos() {
        return foto_evidencia_inicial_dos.replace("%", "%25").replace("&", "%26");
    }

    public void setFoto_evidencia_inicial_dos(String foto_evidencia_inicial_dos) {
        this.foto_evidencia_inicial_dos = foto_evidencia_inicial_dos;
    }

    public String getFoto_evidencia_final_uno() {
        return foto_evidencia_final_uno.replace("%", "%25").replace("&", "%26");
    }

    public void setFoto_evidencia_final_uno(String foto_evidencia_final_uno) {
        this.foto_evidencia_final_uno = foto_evidencia_final_uno;
    }

    public String getFoto_evidencia_final_dos() {
        return foto_evidencia_final_dos.replace("%", "%25").replace("&", "%26");
    }

    public void setFoto_evidencia_final_dos(String foto_evidencia_final_dos) {
        this.foto_evidencia_final_dos = foto_evidencia_final_dos;
    }

    public String getFirmavobo() {
        return firmavobo.replace("%", "%25").replace("&", "%26");
    }

    public void setFirmavobo(String firmavobo) {
        this.firmavobo = firmavobo;
    }

    public String getFoto_ine_anverso() {
        return foto_ine_anverso.replace("%", "%25").replace("&", "%26");
    }

    public void setFoto_ine_anverso(String foto_ine_anverso) {
        this.foto_ine_anverso = foto_ine_anverso;
    }

    public String getFoto_ine_reverso() {
        return foto_ine_reverso.replace("%", "%25").replace("&", "%26");
    }

    public void setFoto_ine_reverso(String foto_ine_reverso) {
        this.foto_ine_reverso = foto_ine_reverso;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public Reporte(){}


    public void imprimirReporte(){
        System.out.println("Usuario: " + getUsuario());
        System.out.println("Folio: " + getFolio());
        System.out.println("Dirección: " + getDireccion());
        System.out.println("Latitud: " + getLatitud());
        System.out.println("Longitud: " + getLatitud());
        System.out.println("Foto evidencia inicial uno: " + getFoto_evidencia_inicial_uno());
        System.out.println("Foto evidencia inicial dos: " + getFoto_evidencia_inicial_dos());
        System.out.println("Foto evidencia final uno: " + getFoto_evidencia_final_uno());
        System.out.println("Foto evidencia final dos: " + getFoto_evidencia_final_dos());
        System.out.println("Firma VOBO: " + getFirmavobo());
        System.out.println("Foto ine anverso: " + getFoto_ine_anverso());
        System.out.println("Foto ine reverso: " + getFoto_ine_reverso());


    }
}
