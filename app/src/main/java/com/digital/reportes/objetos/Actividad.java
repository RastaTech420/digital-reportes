package com.digital.reportes.objetos;

public class Actividad {
	private String descripcion;
	private String evidenciaFoto;
	
	public Actividad(){}
	
	public Actividad(String descripcion, String evidenciaFoto) {
		this.descripcion = descripcion;
		this.evidenciaFoto = evidenciaFoto;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getEvidenciaFoto() {
		return evidenciaFoto;
	}
	
	public void setEvidenciaFoto(String evidenciaFoto) {
		this.evidenciaFoto = evidenciaFoto;
	}
}
