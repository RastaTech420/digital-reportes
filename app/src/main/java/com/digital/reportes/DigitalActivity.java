package com.digital.reportes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.digital.reportes.FragmentsMain.NotificacionesFragment;
import com.digital.reportes.ReportesFragment.DetalleReporteMoctezumaFragment;
import com.digital.reportes.ReportesFragment.ReporteListadoMoctezumaFragment;
import com.digital.reportes.ReportesFragment.ReporteListadoSupervisorFragment;
import com.digital.reportes.vistasDigital.TrackingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DigitalActivity extends AppCompatActivity implements
        TrackingFragment.OnFragmentInteractionListener,
        NotificacionesFragment.OnFragmentInteractionListener,
        ReporteListadoMoctezumaFragment.OnFragmentInteractionListener,
        ReporteListadoSupervisorFragment.OnFragmentInteractionListener,
        DetalleReporteMoctezumaFragment.OnFragmentInteractionListener {

    BottomNavigationView navView;
    public static String id, nombre_usuario, tipo_usuario;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_digital_ordenes:
                Fragment ordenes = new ReporteListadoMoctezumaFragment();
                switchFragments(ordenes);
                return true;
            case R.id.navigation_digital_reportes:
                Fragment reportes = new ReporteListadoSupervisorFragment();
                switchFragments(reportes);
                return true;

            case R.id.navigation_digital_seguimiento:
                Fragment tracking = new TrackingFragment();
                switchFragments(tracking);
                return true;


            case R.id.navigation_digital_notificaciones:
                Fragment notificacion = new NotificacionesFragment();
                switchFragments(notificacion);
                return true;

            default:
                break;
        }
        return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital);

        navView = findViewById(R.id.nav_view_digital);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.getMenu().clear();
        navView.inflateMenu(R.menu.bottom_nav_menu_digital);

        datosLogin();

        Fragment ordenes = new ReporteListadoMoctezumaFragment();
        fragmentInicial(R.id.contenedor_principal_digital,ordenes );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_toolbar) {
            Intent i = new Intent(DigitalActivity.this, LoginActivity.class);
            overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal_digital, fragment);
        ft.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void datosLogin() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        assert bundle != null;
        id             = bundle.getString("id");
        nombre_usuario = bundle.getString("nombre");
        tipo_usuario   = bundle.getString("tipo_usuario");
        setTitle("Bienvenido "+nombre_usuario);
    }
    private void fragmentInicial(int id, Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(id, fragment);
        ft.commit();
    }
}

