package com.digital.reportes.FragmentsCargaReporte;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.digital.reportes.BuildConfig;
import com.digital.reportes.CargaReportesActivity;
import com.digital.reportes.R;
import com.digital.reportes.adaptadores.AdaptadorActividades;
import com.digital.reportes.adaptadores.AdaptadorActividadesRealizar;
import com.digital.reportes.interfaces.OnClickImageListener;
import com.digital.reportes.objetos.Actividad;
import com.digital.reportes.objetos.ActividadesRealizarPOJO;
import com.digital.reportes.objetos.Constantes;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_FOTO_INICIAL_1;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_FOTO_INICIAL_2;

public class ActividadesRealizarReporteFragment extends Fragment implements OnClickImageListener {


    private OnFragmentInteractionListener mListener;

    private List<ActividadesRealizarPOJO> actividadesRealizarList;
    private AdaptadorActividadesRealizar adaptadorActividadesRealizar;


    Button btnAceptarTareasCargadas, btnSiguienteTareasAdicionales;
    CardView cardTareasCargadas;
    LinearLayout lnSeccionActividadesExtra, lnContenedorActividadesExtra, lnLoading;

    Switch swTareasAdicionales;

    EditText cajaNumActExt;

    RecyclerView recyclerActividadesExtras;



    EditText cajaDescripcionActividadExtra;
    ImageButton btnImagenActividadExtra;
    ImageView imgVistaActividadExtra;
    int SOLICITUD_ACTIVIDAD_EXTRA=21;
    int contadorActividadesExtras=0;
    int casoTipoFotoSeleccion;
    Uri fotoUri;
    private StorageReference storageReference;
    private FirebaseStorage storage;

    ProgressDialog progressDialog;
    String descripcionActiExtra;
    RequestQueue requestQueue;
    StringRequest stringRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_actividades_realizar_reporte, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_tareas_cargadas);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        actividadesRealizarList = new ArrayList<>();
        adaptadorActividadesRealizar = new AdaptadorActividadesRealizar(actividadesRealizarList, getActivity());
        recyclerView.setAdapter(adaptadorActividadesRealizar);

        btnAceptarTareasCargadas = view.findViewById(R.id.btnAceptarTareasCargadas);
        cardTareasCargadas       = view.findViewById(R.id.cardTareasCargadas);
        lnSeccionActividadesExtra= view.findViewById(R.id.lnSeccionActividadesExtra);

        swTareasAdicionales = view.findViewById(R.id.swTareasAdicionales);
        lnContenedorActividadesExtra = view.findViewById(R.id.lnContenedorActividadesExtra);
        btnSiguienteTareasAdicionales = view.findViewById(R.id.btnSiguienteTareasAdicionales);
        cajaNumActExt        = view.findViewById(R.id.cajaNumActividadesExtras);
        recyclerActividadesExtras = view.findViewById(R.id.recyclerActividadesExtras);

        lnLoading = view.findViewById(R.id.lnLoading);

        cajaDescripcionActividadExtra = view.findViewById(R.id.cajitaDescripcionActividadExtra);
        btnImagenActividadExtra= view.findViewById(R.id.btnImagenActividadExtra);
        imgVistaActividadExtra= view.findViewById(R.id.vistaImagenActividadExtra);
        storage = FirebaseStorage.getInstance();

        requestQueue = Volley.newRequestQueue(getActivity());
        progressDialog = new ProgressDialog(getActivity());

        obtenerDatos();
        clicks();

        return view;
    }

    private void obtenerFoto(final int num_foto) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);

        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);


        final AlertDialog dialog = builder.create();

        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=1;

                verificarPermisosCamaraMemoria(num_foto, 1);
                dialog.dismiss();


            }
        });

        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=2;

                verificarPermisosCamaraMemoria(num_foto, 2);

                dialog.dismiss();

            }
        });



        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);

            }
        }


    }

    private void abrirCamara(int foto, int tipoCaso) {

        switch (tipoCaso){

            case 1:

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY, fotoUri);
                startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), SOLICITUD_ACTIVIDAD_EXTRA);

                break;
            case 2:

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
                String currentDateandTime = sdf.format(new Date());

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoactividad_extra_num__"+contadorActividadesExtras+currentDateandTime+".jpg");

                if (getActivity() != null) {
                    fotoUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fotoUri);
                    startActivityForResult(intent, SOLICITUD_ACTIVIDAD_EXTRA);
                }else{
                    Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                }

                break;

        }


    }


    private void clicks() {
        /*----EMPIEZA ACTIVIDAD EXTRAS CÓDIGO------*/
        btnImagenActividadExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cajaDescripcionActividadExtra.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Describa la actividad primeramente.", Toast.LENGTH_SHORT).show();
                }else{
                descripcionActiExtra=cajaDescripcionActividadExtra.getText().toString();
                obtenerFoto(contadorActividadesExtras);

                }

            }
        });


        /*----TERMINA ACTIVIDAD EXTRAS CÓDIGO------*/
        btnAceptarTareasCargadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardTareasCargadas.setVisibility(View.GONE);
                lnSeccionActividadesExtra.setVisibility(View.VISIBLE);
            }
        });

        btnSiguienteTareasAdicionales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment firmaReporte = new FirmaReporteFragment();
                switchFragments(firmaReporte);
            }
        });

        swTareasAdicionales.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    lnContenedorActividadesExtra.setVisibility(View.VISIBLE);
                }else{
                    lnContenedorActividadesExtra.setVisibility(View.GONE);

                }
            }
        });

        cajaNumActExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (cajaNumActExt.getText().toString() != null ) {
                        int numActividades = Integer.parseInt(cajaNumActExt.getText().toString());
                        generarTarjetasActividades(numActividades);

                    }else if( cajaNumActExt.getText().toString() == ""){
                        generarTarjetasActividades(0);

                    }

                }catch (Exception e) {
                    Log.e("ERROR", e.getLocalizedMessage());
                }

            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickImagen(int pos) {
        Uri foto = null;
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_LOCAL_ONLY, foto);

        startActivityForResult(i, 86 );

        /*
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_LOCAL_ONLY, foto);
        startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), 86);
*/
    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void agregarTareasWS(String os_clave, String os_nombre, String paq_nombre, String act_nombre){
        ActividadesRealizarPOJO actividadesRealizarPOJO = new ActividadesRealizarPOJO();
        actividadesRealizarPOJO.setOs_clave(os_clave);
        actividadesRealizarPOJO.setOs_nombre(os_nombre);
        actividadesRealizarPOJO.setAct_nombre(act_nombre);
        actividadesRealizarPOJO.setPaq_nombre(paq_nombre);
        actividadesRealizarList.add(actividadesRealizarPOJO);
        adaptadorActividadesRealizar.notifyDataSetChanged();
    }
    private void obtenerDatos() {
        lnLoading.setVisibility(View.VISIBLE);
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"mostrar_actividades_xfkorden_servicio.php?fkorden="+ Constantes.OBJETO_REPORTE.getClave();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    private void obtieneJSON(String response){
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length(); i++){
                JSONObject js = jsonArray.getJSONObject(i);

                /*String clave = js.getString("os_clave");*/
                String nombre= js.getString("os_nombre");
                String actividad = js.getString("act_nombre");
                String paquete = js.getString("paq_nombre");

                agregarTareasWS(Constantes.OBJETO_REPORTE.getClave(),nombre,paquete,actividad);
                lnLoading.setVisibility(View.GONE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal_reportes, fragment);
        ft.commit();
    }

    private void generarTarjetasActividades(int numActividades) {
        recyclerActividadesExtras.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorActividades adapter;
        List<Actividad> ACTIVIDADES = new ArrayList<>();


        for (int i = 0; i < numActividades; i++) {
            String descripcion ="";
            String foto ="";

            ACTIVIDADES.add(new Actividad(descripcion, foto));
            adapter = new AdaptadorActividades(ACTIVIDADES, getActivity());
            adapter.setOnClickImageListener(this);

            recyclerActividadesExtras.setAdapter(adapter);

        }

    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA


        if (requestCode == SOLICITUD_ACTIVIDAD_EXTRA && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnImagenActividadExtra.setVisibility(View.GONE);
                imgVistaActividadExtra.setVisibility(View.VISIBLE);
                imgVistaActividadExtra.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_ACTIVIDAD_EXTRA,bitmap);



            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if(requestCode == SOLICITUD_ACTIVIDAD_EXTRA && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {

                Bitmap bitmap = decodificarBitmap(getActivity(), fotoUri);
                btnImagenActividadExtra.setVisibility(View.GONE);
                imgVistaActividadExtra.setVisibility(View.VISIBLE);
                imgVistaActividadExtra.setImageBitmap(bitmap);

                subirFotoFirebase(fotoUri,SOLICITUD_ACTIVIDAD_EXTRA,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


    }

    void subirFotoFirebase(Uri uri, final int numFoto, Bitmap foto) {


        storageReference = storage.getReference("reporte_" + Constantes.OBJETO_REPORTE.getClave());//FOTOS SEGÚN SEA EL CASO


        procesoSubidaFirebase(uri, SOLICITUD_ACTIVIDAD_EXTRA, foto);


    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);



        return baos.toByteArray();
    }

    private void procesoSubidaFirebase(Uri uri, final int numFoto, Bitmap foto) {

        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());

        final byte[] data = getImageCompressed(foto);

        UploadTask uploadTask = fotoReferencia.putBytes(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal = task.getResult().toString();

                registrarActividadExtra(uriFinal,descripcionActiExtra, Constantes.OBJETO_REPORTE.getClave());

                progressDialog.hide();
                Toast.makeText(getContext(),"Foto cargada!", Toast.LENGTH_LONG).show();

                btnImagenActividadExtra.setVisibility(View.VISIBLE);
                imgVistaActividadExtra.setVisibility(View.GONE);
                cajaDescripcionActividadExtra.setText("");
            }

        });
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;


                progressDialog.setTitle("Subiendo tu foto");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.show();


                System.out.println("Upload is "+progress+"% done");
            }
        });
    }

    private void registrarActividadExtra(String uriFinal, String descripcionActiExtra, String clave) {
        String url=Constantes.URL_PRINCIPAL +"insertar_act_extra.php?descripcion="+descripcionActiExtra+"&fkrep="+Constantes.CLAVE_REPORTE_VACIO_CREADO+"&evidenciafoto="+(uriFinal.replace("%", "%25").replace("&", "%26"));
        System.out.println("URL EXTRA:  "+url);
        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getContext(), "Actividad subida!!", Toast.LENGTH_SHORT).show();
                fotoUri=null;
                contadorActividadesExtras=contadorActividadesExtras+1;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Se produjo un problema a la hora de subir la actividad . "+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(stringRequest);

    }


}
