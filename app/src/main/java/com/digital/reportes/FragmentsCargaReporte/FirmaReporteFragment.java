package com.digital.reportes.FragmentsCargaReporte;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.digital.reportes.BuildConfig;
import com.digital.reportes.R;
import com.digital.reportes.objetos.Constantes;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_FIRMA;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_INE_ANVERSO;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_INE_REVERSO;

public class FirmaReporteFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    SignaturePad cuadroFirmaRep;
    ImageButton btnFotoInicialUnoINE,btnFotoInicialDosINE;
    ImageView vistaFotoInicialUnoINE,vistaFotoInicialDosINE;
    Button guardarFirmaBoton,limpiarFirmaBoton;
    private Uri uriINEanverso;
    private Uri uriINEreverso;
    int casoTipoFotoSeleccion;


    private FirebaseStorage storage;
    private StorageReference storageReference;
    ProgressDialog progressDialog;

    Button btnSiguienteFirmaReporte;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_firma_reporte, container, false);


        //fotos
        storage = FirebaseStorage.getInstance();

        cuadroFirmaRep          =     view.findViewById(R.id.firmaCuadroReporte);
        btnFotoInicialUnoINE     = view.findViewById(R.id.imgButtonFotoInicialUnoINE);
        vistaFotoInicialUnoINE   = view.findViewById(R.id.imgFotoInicialUnoVistaINE);
        btnFotoInicialDosINE     = view.findViewById(R.id.imgButtonFotoInicialDosINE);
        vistaFotoInicialDosINE   = view.findViewById(R.id.imgFotoInicialDosVistaINE);
        btnSiguienteFirmaReporte = view.findViewById(R.id.btnSiguienteFirmaReporte);
        guardarFirmaBoton        = view.findViewById(R.id.guardarFirmaBoton);
        limpiarFirmaBoton        = view.findViewById(R.id.limpiarFirmaBoton);
        progressDialog = new ProgressDialog(getActivity());


        clicks();
        return view;
    }

    private void clicks() {

        guardarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    subirFotoFirebase(bitmapToUri(getContext(),cuadroFirmaRep.getSignatureBitmap()),SOLICITUD_FIRMA,cuadroFirmaRep.getSignatureBitmap());
                }catch (Exception e){
                    Toast.makeText(getContext(), "Se produjo un problema a intentar subir, intente nuevamente", Toast.LENGTH_LONG).show();
                    System.out.println("Exeption firma" + e.toString() );
                }
            }
        });
        limpiarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cuadroFirmaRep.clear();
            }
        });

        btnFotoInicialUnoINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(5);
            }

        });

        btnFotoInicialDosINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(6);
            }
        });

        btnSiguienteFirmaReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Guardar variable e iniciar el nuevo fragment

                Fragment evidenciaFinal = new EvidenciaFinalReporteFragment();
                switchFragments(evidenciaFinal);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    void subirFotoFirebase(Uri uri, final int numFoto, Bitmap foto)
    {

        storageReference = storage.getReference("reporte_"+Constantes.OBJETO_REPORTE.getClave());//FOTOS SEGÚN SEA EL CASO

        switch (numFoto)
        {
            case SOLICITUD_FIRMA:
                procesoSubidaFirebase(uri, SOLICITUD_FIRMA,foto);
                break;

            case SOLICITUD_INE_ANVERSO:
                procesoSubidaFirebase(uri, SOLICITUD_INE_ANVERSO,foto);
                break;

            case SOLICITUD_INE_REVERSO:
                procesoSubidaFirebase(uri, SOLICITUD_INE_REVERSO,foto);
/*
                if (Constantes.OBJETO_REPORTE.getFirmavobo().isEmpty() == false
                        && Constantes.OBJETO_REPORTE.getFoto_ine_anverso().isEmpty() == false
                        && Constantes.OBJETO_REPORTE.getFoto_ine_reverso().isEmpty() == false)
                {
                    //pasar a la siguiente activiti
                }*/
                break;
        }

    }
    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);



        return baos.toByteArray();
    }


    private void procesoSubidaFirebase(Uri uri, final int numFoto, Bitmap foto) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());

        final byte[] data = getImageCompressed(foto);

        UploadTask uploadTask = fotoReferencia.putBytes(data);


        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal=task.getResult().toString();

                switch (numFoto){

                    case SOLICITUD_FIRMA:

                        Constantes.OBJETO_REPORTE.setFirmavobo(uriFinal);


                        break;

                    case SOLICITUD_INE_ANVERSO:

                        Constantes.OBJETO_REPORTE.setFoto_ine_anverso(uriFinal);


                        break;

                    case SOLICITUD_INE_REVERSO:


                        Constantes.OBJETO_REPORTE.setFoto_ine_reverso(uriFinal);


                        break;

                }


                progressDialog.hide();
                Toast.makeText(getContext(),"Foto cargada!", Toast.LENGTH_LONG).show();


            }
        });
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;


                progressDialog.setTitle("Subiendo tu foto");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.show();


                System.out.println("Upload is "+progress+"% done");
            }
        });
    }



    public Uri bitmapToUri(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "firma_vobo", null);
        return Uri.parse(path);
    }

    private void obtenerFoto(final int num_foto) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);

        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);


        final AlertDialog dialog = builder.create();

        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=1;

                verificarPermisosCamaraMemoria(num_foto, 1);
                dialog.dismiss();


            }
        });

        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=2;

                verificarPermisosCamaraMemoria(num_foto, 2);

                dialog.dismiss();

            }
        });



        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);

            }
        }


    }

    private void abrirCamara(int foto, int tipoCaso) {
        if(foto==5 && tipoCaso == 1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriINEanverso);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_INE_ANVERSO);

        }else if(foto==6 && tipoCaso == 1){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriINEreverso);
            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_INE_REVERSO);

        }else if(foto==5 && tipoCaso == 2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "foto_ine_anverso"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriINEanverso = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriINEanverso);
                startActivityForResult(intent, SOLICITUD_INE_ANVERSO);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }

        }else if(foto==6 && tipoCaso == 2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "foto_ine_reverso"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriINEreverso = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriINEreverso);
                startActivityForResult(intent, SOLICITUD_INE_REVERSO);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }



        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA

         if (requestCode == SOLICITUD_INE_ANVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialUnoINE.setVisibility(View.GONE);
                vistaFotoInicialUnoINE.setVisibility(View.VISIBLE);
                vistaFotoInicialUnoINE.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_INE_ANVERSO,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_INE_ANVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriINEanverso);
                btnFotoInicialUnoINE.setVisibility(View.GONE);
                vistaFotoInicialUnoINE.setVisibility(View.VISIBLE);
                vistaFotoInicialUnoINE.setImageBitmap(bitmap);

                subirFotoFirebase(uriINEanverso,SOLICITUD_INE_ANVERSO,bitmap);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_INE_REVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialDosINE.setVisibility(View.GONE);
                vistaFotoInicialDosINE.setVisibility(View.VISIBLE);
                vistaFotoInicialDosINE.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_INE_REVERSO,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_INE_REVERSO && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriINEreverso);
                btnFotoInicialDosINE.setVisibility(View.GONE);
                vistaFotoInicialDosINE.setVisibility(View.VISIBLE);
                vistaFotoInicialDosINE.setImageBitmap(bitmap);
                subirFotoFirebase(uriINEreverso,SOLICITUD_INE_REVERSO,bitmap);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal_reportes, fragment);
        ft.commit();
    }
}