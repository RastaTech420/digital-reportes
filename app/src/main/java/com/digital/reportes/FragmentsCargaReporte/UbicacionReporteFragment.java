package com.digital.reportes.FragmentsCargaReporte;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.digital.reportes.MainActivity;
import com.digital.reportes.MapsActivity;
import com.digital.reportes.R;
import com.digital.reportes.objetos.Constantes;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class UbicacionReporteFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    Button btnContinuarEjecucion, btnEnviarNoejecucion, btnContinuarMapa, btnCargarDesdeMapa;
    Switch swOrdenEjecucion;
    LinearLayout lnEjecucion, lnNoEjecucion, ln2, ln3, lnSuccess;

    LocationManager locationManager;


    Geocoder geocoder;
    List<Address> addresses;
    Double latitud = null;
    Double longitud= null;
    private String ubicacion;

    private SupportMapFragment mapFrag;
    private GoogleMap map;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ubicacion_reporte, container, false);
        locationManager = (LocationManager)getActivity(). getSystemService(Context.LOCATION_SERVICE);

        ln2 = view.findViewById(R.id.ln2);
        ln3 = view.findViewById(R.id.ln3);
        lnSuccess = view.findViewById(R.id.lnSuccess);
        btnContinuarMapa = view.findViewById(R.id.btnContinuarMapa);

        btnCargarDesdeMapa = view.findViewById(R.id.btnCargarUbicacionMapa);

        swOrdenEjecucion = view.findViewById(R.id.swOrdenEjecucion);
        lnEjecucion       = view.findViewById(R.id.lnEjecucion);
        lnNoEjecucion     = view.findViewById(R.id.lnNoEjecucion);

        btnContinuarEjecucion = view.findViewById(R.id.btnContinuarEjecucion);
        btnEnviarNoejecucion  = view.findViewById(R.id.btnEnviarNoejecucion);

        clicks();
        toggleGPSUpdates();

        loadMap();

        return view;
    }

    private void clicks() {
        btnEnviarNoejecucion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln2.setVisibility(View.GONE);
                lnSuccess.setVisibility(View.VISIBLE);
            }
        });
        btnContinuarEjecucion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ln2.setVisibility(View.GONE);
                ln3.setVisibility(View.VISIBLE);
            }
        });
        btnContinuarMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), Constantes.OBJETO_REPORTE.getDireccion() , Toast.LENGTH_LONG).show();
                Fragment estadoInicial = new EstadoInicialReporteFragment();
                switchFragments(estadoInicial);
            }
        });
        btnCargarDesdeMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(getContext(), MapsActivity.class));
                Intent intent = new Intent(getContext(), MapsActivity.class);
                intent.putExtra("latitudExtra", String.valueOf(MainActivity.latitud));
                intent.putExtra("longitudExtra", String.valueOf(MainActivity.longitud));

                startActivity(intent);
                // linearMapaContenedor.setVisibility(View.VISIBLE);
            }
        });
        swOrdenEjecucion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    lnNoEjecucion.setVisibility(View.GONE);
                    lnEjecucion.setVisibility(View.VISIBLE);

                }else{
                    lnEjecucion.setVisibility(View.GONE);
                    lnNoEjecucion.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal_reportes, fragment);
        ft.commit();
    }
/*
    private void lastKnowLocation()
    {
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(getActivity());
        client.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>()
        {
            @Override
            public void onSuccess(Location location)
            {
                if (location != null)
                {
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();

                    gpsData(latitud, longitud);
                }
            }
        });

    }
*/

    ///-----------

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Activar Ubicación")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                        "usa esta app")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void toggleGPSUpdates( ) {
        if (!checkLocation())
            return;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,2 * 20 * 1000, 10, locationListenerGPS);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10, locationListenerGPS);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,2 * 20 * 1000, 10, locationListenerGPS);

    }


    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            latitud = location.getLatitude();
            longitud = location.getLongitude();

            gpsData(latitud, longitud);


        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }
        @Override
        public void onProviderDisabled(String s) {
        }
    };




    ////------------




    private void gpsData(Double latitud, Double longitud)
    {
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try
        {
            addresses = geocoder.getFromLocation(latitud, longitud, 100);

            String direccion = addresses.get(0).getAddressLine(0);
            //String area      = addresses.get(0).getLocality();
            String ciudad    = addresses.get(0).getAdminArea();
            String codigopostal= addresses.get(0).getPostalCode();
            //latitud = addresses.get(0).getLatitude();
            //longitud= addresses.get(0).getLongitude();


            ubicacion = direccion+", "+codigopostal;


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadMap() {
        GoogleMapOptions options = new GoogleMapOptions();
        options.zOrderOnTop(true);

        mapFrag =  (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapillaNuevoReporte);

        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        map.clear();
                        map.addMarker(new MarkerOptions().position(latLng).title("Mi ubicación actual"));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(latLng, 17);
                        map.animateCamera(animacionZoom);
                        Constantes.OBJETO_REPORTE.setLatitud(String.valueOf(latLng.latitude));
                        Constantes.OBJETO_REPORTE.setLongitud(String.valueOf(latLng.longitude));

                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try
                        {
                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 100);
                            String direccion = addresses.get(0).getAddressLine(0);
                            String codigopostal= addresses.get(0).getPostalCode();
                            String   direccionActual = direccion+", "+codigopostal;
                            Constantes.OBJETO_REPORTE.setDireccion(direccionActual);

                        }catch (Exception e){
                            Toast.makeText(getContext(), "Ha ocurrido un problema a la hora de obtener la dirección actual, por favor intente nuevamente...", Toast.LENGTH_LONG).show();

                        }

                    }
                });


            }
        });
    }

}
