package com.digital.reportes.FragmentsCargaReporte;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.digital.reportes.BuildConfig;
import com.digital.reportes.CargaReportesActivity;
import com.digital.reportes.LoginActivity;
import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import com.digital.reportes.SplashActivity;
import com.digital.reportes.objetos.Constantes;
import com.digital.reportes.objetos.Reporte;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_FOTO_FINAL_1;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_FOTO_FINAL_2;
import static com.digital.reportes.CargaReportesActivity.clave;

public class EvidenciaFinalReporteFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    ImageButton btnFotoFinalUno,btnFotoFinalDos;
    ImageView vistaFotoFinalUno,vistaFotoFinalDos;
    Button btnEnviarReporteFinal;
    RequestQueue requestQueue;
    StringRequest stringRequest;
    private Uri uriImagenFinalUno;
    private Uri uriImagenFinalDos;

    int casoTipoFotoSeleccion;

    private StorageReference storageReference;
    private FirebaseStorage storage;

    CardView cardTrabajoFinalizado;
    LinearLayout lnSuccess;

    ProgressDialog progressDialog;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_evidencia_final_reporte, container, false);

        //fotos
        storage = FirebaseStorage.getInstance();


        requestQueue = Volley.newRequestQueue(getActivity());

        btnFotoFinalUno      = view.findViewById(R.id.imgButtonFotoFinalUno);
        btnFotoFinalDos      = view.findViewById(R.id.imgButtonFotoFinalDos);
        vistaFotoFinalUno    = view.findViewById(R.id.imgFotoFinalUnoVista);
        vistaFotoFinalDos    = view.findViewById(R.id.imgFotoFinalDosVista);
        btnEnviarReporteFinal= view.findViewById(R.id.btnEnviarReporteFinal);
        lnSuccess             = view.findViewById(R.id.lnSuccess);
        cardTrabajoFinalizado = view.findViewById(R.id.cardTrabajoFinalizado);
        storage = FirebaseStorage.getInstance();
        progressDialog = new ProgressDialog(getActivity());



        clicks();
        activarBoton();
        return view;
    }

    private void clicks() {
        vistaFotoFinalUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(1);

            }
        });

        vistaFotoFinalDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(2);

            }
        });


        btnFotoFinalUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(3);
            }
        });
        btnFotoFinalDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(4);
            }
        });
        btnEnviarReporteFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Usuario: " + Constantes.OBJETO_REPORTE.getUsuario());
                System.out.println("Folio: " + Constantes.OBJETO_REPORTE.getFolio());
                System.out.println("Dirección: " + Constantes.OBJETO_REPORTE.getDireccion());
                System.out.println("Latitud: " + Constantes.OBJETO_REPORTE.getLatitud());
                System.out.println("Longitud: " + Constantes.OBJETO_REPORTE.getLatitud());
                System.out.println("Foto evidencia inicial uno: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_uno());
                System.out.println("Foto evidencia inicial dos: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_dos());
                System.out.println("Foto evidencia final uno: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_final_uno());
                System.out.println("Foto evidencia final dos: " + Constantes.OBJETO_REPORTE.getFoto_evidencia_final_dos());
                System.out.println("Firma VOBO: " + Constantes.OBJETO_REPORTE.getFirmavobo());
                System.out.println("Foto ine anverso: " + Constantes.OBJETO_REPORTE.getFoto_ine_anverso());
                System.out.println("Foto ine reverso: " + Constantes.OBJETO_REPORTE.getFoto_ine_reverso());

                try{
                    enviarRegistroBD(Constantes.OBJETO_REPORTE);
                    //ln8.setVisibility(View.GONE);
                    //tvNoOrden.setVisibility(View.GONE);
                    cambiarStatus();
                    cardTrabajoFinalizado.setVisibility(View.GONE);
                    lnSuccess.setVisibility(View.VISIBLE);

                    //Enviar al listado
                    try{
                    int secondsDelayed = 1;
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                            //startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
                            getActivity().finish();
                        }
                    }, secondsDelayed * 2000);
                }catch (Exception e){
                    Log.e("Error al regresar main", e.getLocalizedMessage());
                }

                }catch (Exception e){
                    Toast.makeText(getContext(), "Se produjo un problema a intentar subir, intente nuevamente", Toast.LENGTH_LONG).show();
                }
                //showListClass();


            }
        });
    }

    private void showListClass(){
        try {

            int secondsDelayed = 1;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    startActivity(new Intent(getActivity().getApplicationContext(), MainActivity.class));
                    getActivity().finish();
                }
            }, secondsDelayed * 1000);
        }catch (Exception e){
            Log.e("Error al regresar main", e.getLocalizedMessage());
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //ANTES DE ENVIAR ASEGURARSE DE PASAR AL OBJETO REPORTE "setOrden"
    private void enviarRegistroBD(Reporte objetoReporte) {
        
        actualizarDatosReporteVacioCreado(objetoReporte, Constantes.CLAVE_REPORTE_VACIO_CREADO);

        /*String url= Constantes.URL_PRINCIPAL +"insertar_reporte.php?rep_folio="+Constantes.OBJETO_REPORTE.getFolio()+"&rep_direccion="+Constantes.OBJETO_REPORTE.getDireccion()+"&rep_latitud="+Constantes.OBJETO_REPORTE.getLatitud()+"&rep_longitud="+Constantes.OBJETO_REPORTE.getLongitud()+"&rep_evidencia_inicial_uno="+Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_uno()+"&rep_evidencia_inicial_dos="+Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_dos()+"&rep_evidencia_final_uno="+Constantes.OBJETO_REPORTE.getFoto_evidencia_final_uno()+"&rep_evidencia_final_dos="+Constantes.OBJETO_REPORTE.getFoto_evidencia_final_dos()+"&rep_firmavobo="+Constantes.OBJETO_REPORTE.getFirmavobo()+"&rep_ine_anverso="+Constantes.OBJETO_REPORTE.getFoto_ine_anverso()+"&rep_ine_reverso="+Constantes.OBJETO_REPORTE.getFoto_ine_reverso()+"&fkorden="+Constantes.OBJETO_REPORTE.getClave();
        url= url.replace(" ", "%20");

        System.out.println("URL A EJECUTAR: " +url);
        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getContext(), "Reporte realizado!!", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Se produjo un problema a la hora de realizar el registro. "+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(stringRequest);*/

    }

    private void actualizarDatosReporteVacioCreado(Reporte objetoReporte, String claveReporteVacioCreado) {
        String url= Constantes.URL_PRINCIPAL +"actualizar_reporte.php?rep_folio="+Constantes.OBJETO_REPORTE.getFolio()+"&rep_direccion="+Constantes.OBJETO_REPORTE.getDireccion()+"&rep_latitud="+Constantes.OBJETO_REPORTE.getLatitud()+"&rep_longitud="+Constantes.OBJETO_REPORTE.getLongitud()+"&rep_evidencia_inicial_uno="+Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_uno()+"&rep_evidencia_inicial_dos="+Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_dos()+"&rep_evidencia_final_uno="+Constantes.OBJETO_REPORTE.getFoto_evidencia_final_uno()+"&rep_evidencia_final_dos="+Constantes.OBJETO_REPORTE.getFoto_evidencia_final_dos()+"&rep_firmavobo="+Constantes.OBJETO_REPORTE.getFirmavobo()+"&rep_ine_anverso="+Constantes.OBJETO_REPORTE.getFoto_ine_anverso()+"&rep_ine_reverso="+Constantes.OBJETO_REPORTE.getFoto_ine_reverso()+"&fkorden="+Constantes.OBJETO_REPORTE.getClave()+"&orden_creada="+Constantes.CLAVE_REPORTE_VACIO_CREADO;
        url= url.replace(" ", "%20");

        System.out.println("URL A EJECUTAR: " +url);
        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getContext(), "Reporte realizado!!", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Se produjo un problema a la hora de realizar el registro. "+error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(stringRequest);



    }

    private void obtenerFoto(final int num_foto) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);

        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);


        final AlertDialog dialog = builder.create();

        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=1;

                verificarPermisosCamaraMemoria(num_foto, 1);
                dialog.dismiss();


            }
        });

        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=2;

                verificarPermisosCamaraMemoria(num_foto, 2);

                dialog.dismiss();

            }
        });



        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }
    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);

            }
        }


    }
    private void abrirCamara(int foto, int tipoCaso) {

        if (foto == 3 && tipoCaso == 1) {

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, uriImagenFinalUno);
            startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), SOLICITUD_FOTO_FINAL_1);

        } else if (foto == 4 && tipoCaso == 1) {

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, uriImagenFinalDos);
            startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), SOLICITUD_FOTO_FINAL_2);

        }

        else if(foto==3 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_uno"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenFinalUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalUno);
                startActivityForResult(intent, SOLICITUD_FOTO_FINAL_1);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }else if(foto==4 && tipoCaso == 2) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_dos" + currentDateandTime + ".jpg");

            if (getActivity() != null) {
                uriImagenFinalDos = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalDos);
                startActivityForResult(intent, SOLICITUD_FOTO_FINAL_2);
            } else {
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA

        if (requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoFinalUno.setVisibility(View.VISIBLE);
                vistaFotoFinalUno.setImageBitmap(bitmap);


                subirFotoFirebase(u,SOLICITUD_FOTO_FINAL_1,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if(requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalUno);
                btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoFinalUno.setVisibility(View.VISIBLE);
                vistaFotoFinalUno.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenFinalUno,SOLICITUD_FOTO_FINAL_1,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoFinalDos.setVisibility(View.GONE);
                vistaFotoFinalDos.setVisibility(View.VISIBLE);
                vistaFotoFinalDos.setImageBitmap(bitmap);

                subirFotoFirebase(u,SOLICITUD_FOTO_FINAL_2,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalDos);
                btnFotoFinalDos.setVisibility(View.GONE);
                vistaFotoFinalDos.setVisibility(View.VISIBLE);
                vistaFotoFinalDos.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenFinalDos,SOLICITUD_FOTO_FINAL_2,bitmap);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }

    void subirFotoFirebase(Uri uri, final int numFoto, Bitmap foto) {

        storageReference = storage.getReference("reporte_" + Constantes.OBJETO_REPORTE.getClave());//FOTOS SEGÚN SEA EL CASO
        switch (numFoto) {
            case SOLICITUD_FOTO_FINAL_1:

                procesoSubidaFirebase(uri,SOLICITUD_FOTO_FINAL_1,foto);

                break;
            case SOLICITUD_FOTO_FINAL_2:

                procesoSubidaFirebase(uri,SOLICITUD_FOTO_FINAL_2,foto);


                break;
        }
    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);



        return baos.toByteArray();
    }

    private void procesoSubidaFirebase(Uri uri, final int numFoto, Bitmap foto) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());

        final byte[] data = getImageCompressed(foto);

        UploadTask uploadTask = fotoReferencia.putBytes(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal = task.getResult().toString();

                switch (numFoto) {
                    case SOLICITUD_FOTO_FINAL_1:
                        Constantes.OBJETO_REPORTE.setFoto_evidencia_final_uno(uriFinal);


                        break;
                    case SOLICITUD_FOTO_FINAL_2:

                        Constantes.OBJETO_REPORTE.setFoto_evidencia_final_dos(uriFinal);

                        break;
                }
                progressDialog.hide();
                activarBoton();
                btnEnviarReporteFinal.setEnabled(true);

                Toast.makeText(getContext(),"Foto cargada!", Toast.LENGTH_LONG).show();

            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;


                progressDialog.setTitle("Subiendo tu foto");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.show();


                System.out.println("Upload is "+progress+"% done");
            }
        });
    }


        private void activarBoton() {
        if (Uri.EMPTY.equals(uriImagenFinalUno) && Uri.EMPTY.equals(uriImagenFinalDos)){
            btnEnviarReporteFinal.setEnabled(true);
        }else if(!Uri.EMPTY.equals(uriImagenFinalUno) && !Uri.EMPTY.equals(uriImagenFinalDos)){
            btnEnviarReporteFinal.setEnabled(false);
        }
    }

    private void cambiarStatus(){
        // Estados
        // 0 => Pendiente,
        // 1 => Completado,
        // 2 => Cancelado
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());

        Log.e("Fecha Date", currentDateandTime);
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"actualizar_estatus_orden_servicio.php?estatus=1&clave_orden="+ CargaReportesActivity.clave+"&fecha_finaliza="+currentDateandTime;
        client.post(url,null,new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

}
