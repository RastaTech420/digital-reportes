package com.digital.reportes.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.digital.reportes.R;
import com.digital.reportes.objetos.ActividadesRealizarPOJO;

import java.net.CacheRequest;
import java.util.List;

public class AdaptadorActividadesRealizar extends RecyclerView.Adapter<AdaptadorActividadesRealizar.holderActividadesRealizar> implements View.OnClickListener{

    private List<ActividadesRealizarPOJO> actividadesRealizarList;
    private Context context;
    private View.OnClickListener listener;

    public AdaptadorActividadesRealizar(List<ActividadesRealizarPOJO> actividadesRealizarList, Context context, View.OnClickListener listener) {
        this.actividadesRealizarList = actividadesRealizarList;
        this.context = context;
        this.listener = listener;
    }

    public AdaptadorActividadesRealizar(List<ActividadesRealizarPOJO> actividadesRealizarList, Context context) {
        this.actividadesRealizarList = actividadesRealizarList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdaptadorActividadesRealizar.holderActividadesRealizar onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_tareas_cargadas,parent, false);
        view.setOnClickListener(this);
        return new AdaptadorActividadesRealizar.holderActividadesRealizar(view);
    }
    static class holderActividadesRealizar extends RecyclerView.ViewHolder{

        Context context;
        CardView cardView;
        CheckBox checkBoxActividad;
        public holderActividadesRealizar(@NonNull View itemView) {
            super(itemView);
            context           = itemView.getContext();
            cardView          = itemView.findViewById(R.id.cardTareasCargadas);
            checkBoxActividad = itemView.findViewById(R.id.checkboxTareasCargadas);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull AdaptadorActividadesRealizar.holderActividadesRealizar holder, int position) {
        holder.checkBoxActividad.setText(actividadesRealizarList.get(position).getAct_nombre());
    }

    @Override
    public int getItemCount() {
        return actividadesRealizarList.size();
    }

    @Override
    public void onClick(View view) {
        if (listener!=null){
            listener.onClick(view);
        }
    }
}
