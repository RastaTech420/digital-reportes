package com.digital.reportes.adaptadores;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import com.digital.reportes.ReportesFragment.DetalleReporteMoctezumaFragment;
import com.digital.reportes.VistaMoctezumaActivity;
import com.digital.reportes.objetos.Reporte;
import com.digital.reportes.objetos.ReporteMoctezuma;

import java.util.List;

public class AdaptadorListadoReporteMoctezuma extends RecyclerView.Adapter<AdaptadorListadoReporteMoctezuma.holderAdaptadorListadoReportesMoctezuma>
implements View.OnClickListener{

    private List<Reporte> reporteMoctezumaList;
    private Context context;
    private View.OnClickListener listener;

    private AdaptadorListadoReporteMoctezuma(){}



    public AdaptadorListadoReporteMoctezuma(List<Reporte> reporteMoctezumaList, Context context, View.OnClickListener listener) {
        this.reporteMoctezumaList = reporteMoctezumaList;
        this.context = context;
        this.listener = listener;
    }
    public AdaptadorListadoReporteMoctezuma(List<Reporte> reporteMoctezumaList, Context context) {
        this.reporteMoctezumaList = reporteMoctezumaList;
        this.context = context;
    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }
    @Override
    public void onClick(View view) {
        if (listener!=null){
            listener.onClick(view);
        }
    }

    @NonNull
    @Override
    public AdaptadorListadoReporteMoctezuma.holderAdaptadorListadoReportesMoctezuma onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_listado_moctezuma,parent, false);
        view.setOnClickListener(this);
        return new AdaptadorListadoReporteMoctezuma.holderAdaptadorListadoReportesMoctezuma(view);
    }
    static class holderAdaptadorListadoReportesMoctezuma extends RecyclerView.ViewHolder{
        Context context;
        CardView cardView;
        TextView tvCardLMFolio, tvCardLMDireccion, tvCardLMFecha;

        public holderAdaptadorListadoReportesMoctezuma(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            cardView = itemView.findViewById(R.id.cardListadoMoctezuma);
            tvCardLMFolio = itemView.findViewById(R.id.tvCardLMFolio);
        //    tvCardLMFecha = itemView.findViewById(R.id.tvCardLMFecha);
            tvCardLMDireccion = itemView.findViewById(R.id.tvCardLMDireccion);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull AdaptadorListadoReporteMoctezuma.holderAdaptadorListadoReportesMoctezuma holder, final int position) {
        holder.tvCardLMFolio.setText("Folio: "+reporteMoctezumaList.get(position).getFolio());
        holder.tvCardLMDireccion.setText(reporteMoctezumaList.get(position).getDireccion());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verDetalle(position);
            }
        });
    }

    private void verDetalle(int position) {
        Bundle bundle = new Bundle();
        bundle.putString("clave", reporteMoctezumaList.get(position).getClave());
        bundle.putString("folio", reporteMoctezumaList.get(position).getFolio());
        bundle.putString("direccion", reporteMoctezumaList.get(position).getDireccion());
        bundle.putString("latitud", reporteMoctezumaList.get(position).getLatitud());
        bundle.putString("longitud", reporteMoctezumaList.get(position).getLongitud());
        bundle.putString("evidencia_inicial_1", reporteMoctezumaList.get(position).getFoto_evidencia_inicial_uno());
        bundle.putString("evidencia_inicial_2", reporteMoctezumaList.get(position).getFoto_evidencia_inicial_dos());
        bundle.putString("evidencia_final_1", reporteMoctezumaList.get(position).getFoto_evidencia_final_uno());
        bundle.putString("evidencia_final_2", reporteMoctezumaList.get(position).getFoto_evidencia_final_dos());
        bundle.putString("firma", reporteMoctezumaList.get(position).getFirmavobo());
        bundle.putString("ine_anverso", reporteMoctezumaList.get(position).getFoto_ine_anverso());
        bundle.putString("ine_reverso", reporteMoctezumaList.get(position).getFoto_ine_reverso());
        bundle.putString("orden", reporteMoctezumaList.get(position).getOrden());


        Fragment detalleReporteMoctezumaFragment = new DetalleReporteMoctezumaFragment();
        detalleReporteMoctezumaFragment.setArguments(bundle);

        if (VistaMoctezumaActivity.tipo_usuario.equals("3")){
            ((VistaMoctezumaActivity)context).getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contenedor_vista_moctezuma, detalleReporteMoctezumaFragment)
                    .addToBackStack(null)
                    .commit();
        }else if(MainActivity.tipo_usuario.equals("0")){
            ((MainActivity)context).getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, detalleReporteMoctezumaFragment)
                    .addToBackStack(null)
                    .commit();
        }


    }

    @Override
    public int getItemCount() {
        return reporteMoctezumaList.size();
    }
}
