package com.digital.reportes.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.digital.reportes.CargaReportesActivity;
import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import com.digital.reportes.ReportesFragment.DetalleReporteMoctezumaFragment;
import com.digital.reportes.objetos.Constantes;
import com.digital.reportes.objetos.OrdenesServicioPOJO;


import java.util.List;

public class AdaptadorOrdenesServicio extends RecyclerView.Adapter<AdaptadorOrdenesServicio.holderAdaptadorOrdenesServicio> implements View.OnClickListener {

    private List<OrdenesServicioPOJO> ordenesServicioList;
    private Context context;
    private View.OnClickListener listener;

    private AdaptadorOrdenesServicio(){}

    public AdaptadorOrdenesServicio(List<OrdenesServicioPOJO> ordenesServicioList, Context context){
        this.ordenesServicioList = ordenesServicioList;
        this.context = context;
    }

    public AdaptadorOrdenesServicio(List<OrdenesServicioPOJO> ordenesServicioList, Context context, View.OnClickListener listener) {
        this.ordenesServicioList = ordenesServicioList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener!=null){
            listener.onClick(view);
        }
    }

    @NonNull
    @Override
    public AdaptadorOrdenesServicio.holderAdaptadorOrdenesServicio onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_listado2_moctezuma, parent, false);
        view.setOnClickListener(this);
        return new AdaptadorOrdenesServicio.holderAdaptadorOrdenesServicio(view);
    }

    static class holderAdaptadorOrdenesServicio extends RecyclerView.ViewHolder{
        Context context;
        CardView cardView;
        TextView tvCardLMFolio, tvCardLMNoOrden, tvCardLMDireccion, tvCardLMFecha, tvCardLMFechaFinalizacion, tvCardLMFechaReprogramacion;
        public holderAdaptadorOrdenesServicio(@NonNull View itemView) {
            super(itemView);
            context                   = itemView.getContext();
            cardView                  = itemView.findViewById(R.id.cardListadoMoctezuma);
            tvCardLMFolio             = itemView.findViewById(R.id.tvCardLMFolio);
//            tvCardLMNoOrden           = itemView.findViewById(R.id.tvCardLMNoOrden);
            tvCardLMDireccion         = itemView.findViewById(R.id.tvCardLMDireccion);
            tvCardLMFecha             = itemView.findViewById(R.id.tvCardLMFecha);
            tvCardLMFechaFinalizacion = itemView.findViewById(R.id.tvCardLMFechaFinalizacion);
            tvCardLMFechaReprogramacion = itemView.findViewById(R.id.tvCardLMFechaReprogramacion);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull AdaptadorOrdenesServicio.holderAdaptadorOrdenesServicio holder, final int position) {
        holder.tvCardLMFolio.setText("Clave: "+ordenesServicioList.get(position).getOs_clave());
        holder.tvCardLMFecha.setText("Registrado: "+ordenesServicioList.get(position).getOs_fechaIngreso());
        holder.tvCardLMDireccion.setText(ordenesServicioList.get(position).getOs_nombre());


        if (!ordenesServicioList.get(position).getOs_fechaReprogramacion().equals("null")){
            holder.tvCardLMFechaReprogramacion.setText("Fecha reprogramada: "+ordenesServicioList.get(position).getOs_fechaReprogramacion());
            holder.tvCardLMFechaReprogramacion.setBackgroundColor(Color.YELLOW);
            holder.tvCardLMFechaReprogramacion.setTextColor(Color.BLACK);
            holder.tvCardLMFechaReprogramacion.setVisibility(View.VISIBLE);
            holder.tvCardLMFechaFinalizacion.setVisibility(View.GONE);

        }


        if (!ordenesServicioList.get(position).getOs_fechaFinalizacion().equals("null") & !ordenesServicioList.get(position).getOs_fechaFinalizacion().isEmpty()){
            holder.tvCardLMFechaFinalizacion.setBackgroundColor(Color.GREEN);
        }
        if (!ordenesServicioList.get(position).getOs_fechaFinalizacion().equals("null")){

            holder.tvCardLMFechaFinalizacion.setText("Finalizado: "+ordenesServicioList.get(position).getOs_fechaFinalizacion());
        }else{
            holder.tvCardLMFechaFinalizacion.setText("Aún no ha sido finalizada");
            holder.tvCardLMFechaFinalizacion.setBackgroundColor(Color.RED);
            holder.tvCardLMFechaFinalizacion.setTextColor(Color.WHITE);
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verDetalle(position);
                //ahora se manejará la Clave
                Constantes.OBJETO_REPORTE.setClave(ordenesServicioList.get(position).getOs_clave());

                //aquí se carga un : " " (que así le asigno desde la creación de la lista) -NO IMPORTANTE-
                Constantes.OBJETO_REPORTE.setOrden(ordenesServicioList.get(position).getOs_noOrden());
                Constantes.STATUS_ORDEN_ELEGIDA= ordenesServicioList.get(position).getOs_status();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ordenesServicioList.size();
    }

    private void verDetalle(int position){
        Intent i = new Intent(context, CargaReportesActivity.class);
        i.putExtra("clave", ordenesServicioList.get(position).getOs_clave());
        i.putExtra("nombre", ordenesServicioList.get(position).getOs_nombre());
        i.putExtra("fecha_ingreso", ordenesServicioList.get(position).getOs_fechaIngreso());
        i.putExtra("fecha_programada", ordenesServicioList.get(position).getOs_fechaProgramada());
        i.putExtra("fecha_finalizacion",ordenesServicioList.get(position).getOs_fechaFinalizacion());
        i.putExtra("fecha_reprogramacion", ordenesServicioList.get(position).getOs_fechaReprogramacion());
        i.putExtra("status", ordenesServicioList.get(position).getOs_status());
        i.putExtra("fkAsignado", ordenesServicioList.get(position).getOs_fkasi());
        i.putExtra("orden", ordenesServicioList.get(position).getOs_noOrden());
        i.putExtra("observaciones", ordenesServicioList.get(position).getOs_observaciones());
        i.putExtra("prioridad", ordenesServicioList.get(position).getOs_prioridad());
        Constantes.OBJETO_REPORTE.setOrden(ordenesServicioList.get(position).getOs_clave());
        Constantes.OBJETO_REPORTE.setFolio(ordenesServicioList.get(position).getOs_noOrden());

        context.startActivity(i);



    }

}
