package com.digital.reportes.adaptadores;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.digital.reportes.CargaReportesActivity;
import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import com.digital.reportes.ReportesActivity;
import com.digital.reportes.interfaces.OnClickImageListener;
import com.digital.reportes.objetos.Actividad;

import java.util.List;

import static androidx.core.app.ActivityCompat.startActivityForResult;
import static com.digital.reportes.CargaReportesActivity.SOLICITUD_FOTO_INICIAL_1;

public class AdaptadorActividades extends RecyclerView.Adapter<AdaptadorActividades.ViewHolder>  {

	private OnClickImageListener onClickImageListener;

	public void setOnClickImageListener(OnClickImageListener onClickImageListener) {
		this.onClickImageListener = onClickImageListener;
	}

	public static void onActivityResult(int requestCode, int resultCode, Intent data) {
	}

	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder {
		
		private EditText descripcionActividad;
		private ImageButton btnImagenActividadExt;
		private ImageView vistaImagenActividadExt;
		
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			descripcionActividad = itemView.findViewById(R.id.cajaDescripciónActividadExtra);
			btnImagenActividadExt = itemView.findViewById(R.id.btnImagenEvidenciaActividadExtra);
			vistaImagenActividadExt = itemView.findViewById(R.id.vistaImagenEvidenciaActividadExtra);
			
			
			vistaImagenActividadExt.setVisibility(View.GONE);
		}

	}
	
	public List<Actividad> actividadList;
	public Context context;
	
	public AdaptadorActividades(List<Actividad> actividadLista, Context context) {
		this.actividadList = actividadLista;
		this.context = context;
	}
	
	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_actividad_extra,viewGroup,false);

		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		viewHolder.btnImagenActividadExt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (onClickImageListener != null) {
					onClickImageListener.onClickImagen(i);
				}
/*
				Activity origin = (Activity)context;
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.setType("image/*");
				i.putExtra(Intent.EXTRA_LOCAL_ONLY, foto);


				origin.startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), 86);*/
			}
		});



		
	}




	
	@Override
	public int getItemCount() {
		return actividadList.size();
	}





}
