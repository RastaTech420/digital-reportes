package com.digital.reportes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class EstadisticasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadisticas);
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }catch (Exception e){}

        loadChart();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadChart() {
        BarChart chart = findViewById(R.id.barchart);

        ArrayList NoOfInstall = new ArrayList();
        try {
            NoOfInstall.add(new BarEntry(56f, 0));
            NoOfInstall.add(new BarEntry(61f, 1));
            NoOfInstall.add(new BarEntry(55f, 2));
            NoOfInstall.add(new BarEntry(80f, 3));
            NoOfInstall.add(new BarEntry(30f, 4));
            NoOfInstall.add(new BarEntry(55f, 5));
            NoOfInstall.add(new BarEntry(40f, 6));
            NoOfInstall.add(new BarEntry(42f, 7));
            NoOfInstall.add(new BarEntry(49f, 8));
            NoOfInstall.add(new BarEntry(12f, 9));

            ArrayList year = new ArrayList();

            year.add("Ene");
            year.add("Feb");
            year.add("Mar");
            year.add("Abr");
            year.add("May");
            year.add("Jun");
            year.add("Jul");
            year.add("Ago");
            year.add("Sep");
            year.add("Oct");

            BarDataSet bardataset = new BarDataSet(NoOfInstall, "No de Instalaciones");
            chart.animateY(5000);
            BarData data = new BarData(year, bardataset);
            bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
            chart.setData(data);
        } catch (Exception e) {

        }
    }
}
