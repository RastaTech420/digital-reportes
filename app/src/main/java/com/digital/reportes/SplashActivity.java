package com.digital.reportes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);
                finish();
            }
        }, secondsDelayed * 1000);
    }
}
