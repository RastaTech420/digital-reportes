package com.digital.reportes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.digital.reportes.FragmentsCargaReporte.ActividadesRealizarReporteFragment;
import com.digital.reportes.FragmentsCargaReporte.EstadoInicialReporteFragment;
import com.digital.reportes.FragmentsCargaReporte.EvidenciaFinalReporteFragment;
import com.digital.reportes.FragmentsCargaReporte.FirmaReporteFragment;
import com.digital.reportes.FragmentsCargaReporte.UbicacionReporteFragment;
import com.digital.reportes.ReportesFragment.DetalleReporteMoctezumaFragment;
import com.digital.reportes.ViewReporteFinal.DetalleReporteFragment;
import com.digital.reportes.adaptadores.AdaptadorActividades;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class CargaReportesActivity extends AppCompatActivity
implements UbicacionReporteFragment.OnFragmentInteractionListener,
        EstadoInicialReporteFragment.OnFragmentInteractionListener,
        ActividadesRealizarReporteFragment.OnFragmentInteractionListener,
        FirmaReporteFragment.OnFragmentInteractionListener,
        EvidenciaFinalReporteFragment.OnFragmentInteractionListener,
        DetalleReporteFragment.OnFragmentInteractionListener,
        DetalleReporteMoctezumaFragment.OnFragmentInteractionListener
{

    public static final int SOLICITUD_FOTO_INICIAL_1 = 10;
    public static final int SOLICITUD_FOTO_INICIAL_2 = 11;
    public static final int SOLICITUD_FOTO_FINAL_1 = 12;
    public static final int SOLICITUD_FOTO_FINAL_2 = 13;
    public static final int SOLICITUD_INE_ANVERSO = 15;
    public static final int SOLICITUD_INE_REVERSO = 16;


    public static final int SOLICITUD_FIRMA = 150;

    public static final int COUNTERTASK = 0;

    TextView tvUbicacionActual;

    public static String clave, nombre, fecha_ingreso, fecha_programada,
                         fecha_finalizacion, fecha_reprogramacion, status,
                         fkAsignado, orden, observaciones, prioridad;

    BottomNavigationView navView;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            switch (menuItem.getItemId()) {
                case R.id.navigation_ubicacion_reporte:
                    Fragment ubicacionReporte = new UbicacionReporteFragment();
                    switchFragments(ubicacionReporte);
                    return true;
                case R.id.navigation_inicial_reporte:
                    Fragment inicialReporte = new EstadoInicialReporteFragment();
                    switchFragments(inicialReporte);
                    return true;

                case R.id.navigation_actividades_inicial_reporte:
                    Fragment actividadesInicial = new ActividadesRealizarReporteFragment();
                    switchFragments(actividadesInicial);
                    return true;


                case R.id.navigation_firma_reporte:
                    Fragment firmaReporte = new FirmaReporteFragment();
                    switchFragments(firmaReporte);
                    return true;

                case R.id.navigation_evidencia_final_reporte:
                    Fragment evidenciaFinal = new EvidenciaFinalReporteFragment();
                    switchFragments(evidenciaFinal);
                    return true;

                default:
                    break;
            }

            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carga_reportes);

        navView = findViewById(R.id.nav_view_reportes);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.getMenu().clear();
        navView.inflateMenu(R.menu.bottom_nav_menu_reportes);

        tvUbicacionActual = findViewById(R.id.tvUbicacionActual);
        tvUbicacionActual.setText(MainActivity.direccion_completa);
        tvUbicacionActual.setSelected(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recuperarDatos();
        verificarLayout();

    }

    private void verificarLayout() {
        if (status.equals("0")){
            Fragment ubicacionReporteFragment = new UbicacionReporteFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.contenedor_principal_reportes, ubicacionReporteFragment);
            ft.commit();
        }else if(status.equals("1")){
            navView.setVisibility(View.GONE);
            tvUbicacionActual.setVisibility(View.GONE);
            Fragment detalleReporte = new DetalleReporteMoctezumaFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.contenedor_principal_reportes, detalleReporte);
            ft.commit();
        }


    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal_reportes, fragment);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void recuperarDatos(){
        Intent i = getIntent();
        Bundle bundle = i.getExtras();

        if (bundle!=null){
            clave                = bundle.getString("clave");
            nombre               = bundle.getString("nombre");
            fecha_ingreso        = bundle.getString("fecha_ingreso");
            fecha_programada     = bundle.getString("fecha_programada");
            fecha_finalizacion   = bundle.getString("fecha_finalizacion");
            fecha_reprogramacion = bundle.getString("fecha_reprogramacion");
            status               = bundle.getString("status");
            fkAsignado           = bundle.getString("fkAsignado");
           //orden                = bundle.getString("orden");
            observaciones        = bundle.getString("observaciones");
            prioridad            = bundle.getString("prioridad");
        }




    }



}
