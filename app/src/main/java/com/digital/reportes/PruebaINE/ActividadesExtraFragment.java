package com.digital.reportes.PruebaINE;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.digital.reportes.R;
import com.digital.reportes.adaptadores.AdaptadorActividades;
import com.digital.reportes.objetos.Actividad;

import java.util.ArrayList;
import java.util.List;


public class ActividadesExtraFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    EditText etActividadesExtra;
    RecyclerView recyclerActividadesExtras1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_actividades_extra, container, false);

        etActividadesExtra = view.findViewById(R.id.etActividadesExtras);
        recyclerActividadesExtras1 = view.findViewById(R.id.recyclerActividadesExtras1);

        actions();
        return view;
    }

    private void actions() {
        etActividadesExtra.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (etActividadesExtra.getText().toString() != null ) {
                        int numActividades = Integer.parseInt(etActividadesExtra.getText().toString());
                        generarTarjetasActividades(numActividades);

                    }else if( etActividadesExtra.getText().toString() == ""){
                        generarTarjetasActividades(0);

                    }

                }catch (Exception e) {
                    Log.e("ERROR", e.getLocalizedMessage());
                }

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void generarTarjetasActividades(int numActividades) {
        recyclerActividadesExtras1.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorActividades adapter;
        List<Actividad> ACTIVIDADES = new ArrayList<>();

        for (int i = 0; i < numActividades; i++) {
            String descripcion ="";
            String foto ="";

            ACTIVIDADES.add(new Actividad(descripcion, foto));
            adapter = new AdaptadorActividades(ACTIVIDADES, getActivity());
            recyclerActividadesExtras1.setAdapter(adapter);

        }

    }
}
