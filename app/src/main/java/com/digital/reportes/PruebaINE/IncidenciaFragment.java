package com.digital.reportes.PruebaINE;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.digital.reportes.R;

public class IncidenciaFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    Button guardarFirmaBotonIncidencia;
    LinearLayout lnSuccessIncidencia, contendor_incidencia;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_incidencia, container, false);

        initComponents(view);
        clicks();
        return view;
    }



    private void initComponents(View view) {
        guardarFirmaBotonIncidencia = view.findViewById(R.id.guardarFirmaBotonIncidencia);
        lnSuccessIncidencia =view.findViewById(R.id.lnSuccessIncidencia);
        contendor_incidencia = view.findViewById(R.id.contenedor_incidencia);
    }

    private void clicks() {
        guardarFirmaBotonIncidencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contendor_incidencia.setVisibility(View.GONE);
                lnSuccessIncidencia.setVisibility(View.VISIBLE);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
