package com.digital.reportes.PruebaINE;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.digital.reportes.BuildConfig;
import com.digital.reportes.MainActivity;
import com.digital.reportes.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import static android.app.Activity.RESULT_OK;

public class INEFragment extends Fragment {
    
    View view;
    private static final int SOLICITUD_FOTO_INICIAL_1 = 10;
    private static final int SOLICITUD_FOTO_INICIAL_2 = 11;
    private Uri uriImagenIncialUno;
    private Uri uriImagenIncialDos;
    ImageButton btnFotoInicialUnoINE,btnFotoInicialDosINE;
    ImageView vistaFotoInicialUnoINE,vistaFotoInicialDosINE;
    Button guardarFirmaBotonINE, btnIncidencia, btnActExtra;
    LinearLayout lnINE, lnSuccessINE;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ine, container, false);
        initElements();
        clicks();
        
        return view;
    }
    
    private void initElements() {
        btnFotoInicialUnoINE     = view.findViewById(R.id.imgButtonFotoInicialUnoINE);
        vistaFotoInicialUnoINE   = view.findViewById(R.id.imgFotoInicialUnoVistaINE);
        btnFotoInicialDosINE     = view.findViewById(R.id.imgButtonFotoInicialDosINE);
        vistaFotoInicialDosINE   = view.findViewById(R.id.imgFotoInicialDosVistaINE);

        guardarFirmaBotonINE     =view.findViewById(R.id.guardarFirmaBotonINE);
        lnINE                    =view.findViewById(R.id.lnINE);
        lnSuccessINE             = view.findViewById(R.id.lnSuccessINE);
        btnIncidencia            = view.findViewById(R.id.btnIncidencia);
        btnActExtra              = view.findViewById(R.id.btnActExtra);
    }
    
    private void clicks(){
        btnFotoInicialUnoINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(1);
            }
            
        });
        
        btnFotoInicialDosINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerFoto(2);
            }
        });

        guardarFirmaBotonINE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnINE.setVisibility(View.GONE);
                lnSuccessINE.setVisibility(View.VISIBLE);
            }
        });
        btnIncidencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IncidenciaFragment incidenciaFragment = new IncidenciaFragment();
                switchFragments(incidenciaFragment);
            }
        });
        btnActExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadesExtraFragment actividadesExtraFragment = new ActividadesExtraFragment();
                switchFragments(actividadesExtraFragment);
            }
        });

        
        
    }
    
    private void obtenerFoto(final int num_foto) {
        verificarPermisosCamaraMemoria(num_foto);
    }
    
    private void verificarPermisosCamaraMemoria(int num_foto) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto);
            }
        }
        
    }
    
    private void abrirCamara(int foto) {
        if(foto==1){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());
            
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "ine_uno"+currentDateandTime+".jpg");
            
            if (getActivity() != null) {
                uriImagenIncialUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialUno);
                startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_1);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }else  if(foto==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "ine_dos"+currentDateandTime+".jpg");
            if (getActivity() != null){
                uriImagenIncialDos = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialDos);
                startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_2);
            }
            
        }
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA
    
         if(requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK ){
           try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialUno);
                btnFotoInicialUnoINE.setVisibility(View.GONE);
                vistaFotoInicialUnoINE.setVisibility(View.VISIBLE);
                vistaFotoInicialUnoINE.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK) {
             try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialDos);
                btnFotoInicialDosINE.setVisibility(View.GONE);
                vistaFotoInicialDosINE.setVisibility(View.VISIBLE);
                vistaFotoInicialDosINE.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    
    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;
        
        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;
        
        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void switchFragments(Fragment fragment) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.contenedor_principal, fragment);
        ft.commit();
    }

}
